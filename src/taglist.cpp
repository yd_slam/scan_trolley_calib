#include <iostream>
#include <string>
#include <pcl/io/ply_io.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl/point_types.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/common/transforms.h>
using namespace std;
using namespace pcl;
#define NONE       "\e[0m"
#define L_RED      "\e[1;31m"
#define L_GREEN    "\e[1;32m"
#define printlr(format, arg...) do{printf(L_RED format NONE,## arg);}while(0)
#define printlg(format, arg...) do{printf(L_GREEN format NONE,## arg);}while(0)
typedef pcl::PointCloud<pcl::PointXYZRGB> PointCloudT;
ofstream tagposefile;
int n=0;
pcl::PointCloud<pcl::PointXYZRGB> centers;
struct callback_args
{
    PointCloudT::Ptr clicked_points_3d;
    pcl::visualization::PCLVisualizer::Ptr viewerPtr;
};
void pp_callback(const pcl::visualization::PointPickingEvent &event, void *args) //,std::vector<PointT> &selectP)
{
    struct callback_args *data = (struct callback_args *)args;
    if (event.getPointIndex() == -1)
        return;
    pcl::PointXYZRGB current_point;
    event.getPoint(current_point.x, current_point.y, current_point.z);
    data->clicked_points_3d->points.push_back(current_point);
    pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZRGB> red(data->clicked_points_3d, 255, 0, 0);
    data->viewerPtr->removePointCloud("clicked_points");
    data->viewerPtr->addPointCloud(data->clicked_points_3d, red, "clicked_points");
    data->viewerPtr->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 8, "clicked_points");
    printlg("%d: %f %f %f\n",n++,current_point.x,current_point.y,current_point.z);
    tagposefile << current_point.x << " " << current_point.y << " " << current_point.z << std::endl;
    centers.push_back(current_point);
}
//./taglist ../Map/map.ply
int main(int argc, char **argv)
{
    std::string mappath = argv[1];
    tagposefile.open("../config/tagslist.txt");
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGB>());
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer("viewer"));
    if (pcl::io::loadPLYFile(mappath, *cloud))
    {

        printlr("ERROR: Cannot open %s\n",mappath.c_str());
        return -1;
    }
    viewer->addPointCloud(cloud, "bunny");
    viewer->setCameraPosition(0, 0, -2, 0, -1, 0, 0);
    viewer->setBackgroundColor(125, 125, 125);
    viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "bunny");
    struct callback_args cb_args;
    PointCloudT::Ptr clicked_points_3d(new PointCloudT);
    cb_args.clicked_points_3d = clicked_points_3d;
    cb_args.viewerPtr = pcl::visualization::PCLVisualizer::Ptr(viewer);
    viewer->registerPointPickingCallback(pp_callback, (void *)&cb_args);
    std::cout << "Notice: if there is nothing, press 'R' for show." << std::endl;
    std::cout << "Shift+click on three floor points, then press 'Q'..." << std::endl;
    viewer->spin();
    while (!viewer->wasStopped())
    {
        viewer->spinOnce(100);
        boost::this_thread::sleep(boost::posix_time::microseconds(100000));
    }

    tagposefile.close();
    pcl::io::savePLYFile("../temp/tagslist.ply", centers);
    return 0;
}