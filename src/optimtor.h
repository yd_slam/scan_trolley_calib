#ifndef OPTIMTOR_H
#define OPTIMTOR_H
#include "opencv2/opencv.hpp"
#include <cmath>
#include "Eigen/Dense"
#include "Eigen/LU"
#include "Eigen/Core"
#include "eigen3/Eigen/Dense"
#include "opencv2/core/eigen.hpp"
#include <opencv2/core/utility.hpp>
#include <dirent.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl/point_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include "pcl/features/normal_3d.h"
#include <pcl/filters/frustum_culling.h>
#include <pcl/sample_consensus/ransac.h>
#include <pcl/sample_consensus/sac_model_plane.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/filters/project_inliers.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/surface/mls.h>
using namespace std;
using namespace pcl;
using namespace cv;

class Optim
{
public:
    bool Init(std::string _datapath,std::string _predatapath,std::string _outputdatapath,std::string _devicetype);
    bool Optimization();
    void Render();
private:
    int ReadKeysInfo(std::string txtfilename,std::vector<cv::Point2d> &keys2D,std::vector<cv::Point3d> &keys3D);
    double NDT_Process(pcl::PointCloud<pcl::PointXYZ> &Input, pcl::PointCloud<pcl::PointXYZ> &Target, Eigen::Matrix4d &trans_d);
    void CalibNewcam_lidar(cv::Mat cam_lidar_exRT,cv::Mat cam_map_exRT,cv::Mat ndtMat,cv::Mat &newEXMat,cv::Mat &lidar_map_exRT);
    double OptimizeByMap(std::string name,cv::Mat cam_lidar_exRT,cv::Mat cam_map_exRT,cv::Mat &newMat);
private:
    cv::Mat Frontimg,Rearimg;
    std::string datapath,Rkeys2d3dtxt,Lkeys2d3dtxt,outputdatapath;
    pcl::PointCloud<pcl::PointXYZ> pc_temp, lidar_frame,map;
    pcl::PointCloud<pcl::PointXYZRGB> color_mapall,color_map;
    cv::Mat rear_lidar_exRT,front_lidar_exRT,omni_lidar_exRT=cv::Mat::eye(4, 4, CV_64F);
    cv::Mat optim_rear_lidar_exRT,optim_front_lidar_exRT,optim_omni_lidar_exRT=cv::Mat::eye(4, 4, CV_64F);
    cv::Mat rear_map_exRT,front_map_exRT,omni_map_exRT=cv::Mat::eye(4, 4, CV_64F);
    cv::Mat lidar_map_exRT=cv::Mat::eye(4, 4, CV_64F);
    cv::Mat rearproject,frontproject,omniproject=cv::Mat::eye(3, 3, CV_64F);
    cv::Mat reardistortion,frontdistortion,omnidistortion,Edistor_matrix=cv::Mat::zeros(4, 1, CV_64F);
    cv::Size imagesize;
    double xi;
    std::vector<cv::Point2d> Fkeys2D,Rkeys2D,Okeys2D;
    std::vector<cv::Point3d> Fkeys3D,Rkeys3D,OKeys3D;
    pcl::PointCloud<pcl::PointXYZ> Rframe_in_map,Fframe_in_map,Oframe_in_map;
    pcl::PointCloud<pcl::PointXYZ> ndtFrame,neighbors;
    int devicetypeid;



};
#endif
