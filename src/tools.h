#ifndef TOOLS_H
#define TOOLS_H
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <dirent.h>
#include <Eigen/Core>
#include "eigen3/Eigen/Dense"
#include <opencv2/core.hpp>
#include "opencv2/core/eigen.hpp"
#include <opencv2/core/utility.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/core/types.hpp>
#include "opencv2/ccalib/multicalib.hpp"
//均匀采样
#include <pcl/keypoints/uniform_sampling.h>
//ndt
#include <pcl/PCLPointCloud2.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/common/transforms.h>
#include <pcl/registration/ndt.h>
#include <pclomp/ndt_omp.h>
#include <pcl/registration/gicp.h>

using namespace cv;
using namespace std;
using namespace pcl;

void M4ftoM4d(Eigen::Matrix4f &transformation, Eigen::Matrix4d &trans_d);
void SplitString(const std::string &s, std::vector<std::string> &v, const std::string &c)
{
    std::string::size_type pos1, pos2;
    pos2 = s.find(c);
    pos1 = 0;
    while (std::string::npos != pos2)
    {
        v.push_back(s.substr(pos1, pos2 - pos1));

        pos1 = pos2 + c.size();
        pos2 = s.find(c, pos1);
    }
    if (pos1 != s.length())
        v.push_back(s.substr(pos1));
};
void readFileList(std::string path, std::vector<std::string> &files, std::string ext)
{
    struct dirent *ptr;
    DIR *dir;
    dir = opendir(path.c_str());
    while ((ptr = readdir(dir)) != NULL)
    {
        if (ptr->d_name[0] == '.')
            continue;
        std::vector<std::string> strVec;
        SplitString(ptr->d_name, strVec, ".");

        if (strVec.size() >= 2)
        {
            std::string p;
            if (strVec[strVec.size() - 1].compare(ext) == 0)
                files.push_back(p.assign(path).append("/").append(ptr->d_name));
        }
        else
        {
            std::string p;
            readFileList(p.assign(path).append("/").append(ptr->d_name), files, ext);
        }
    }
    closedir(dir);
};
void ReadSonFileList(std::string path, std::vector<std::string> &files)
{
    struct dirent *ptr;
    DIR *dir;
    dir = opendir(path.c_str());
    while((ptr = readdir(dir)) != NULL){
        if (strcmp(ptr->d_name, ".") != 0 && strcmp(ptr->d_name, "..") != 0)
        {
            files.push_back(ptr->d_name);
        }
    }
    closedir(dir);
};
int YDGetPCDDataFilePath(std::string strFileDirectory,
                         std::vector<std::string> &pcdfiles)
{
    readFileList(strFileDirectory, pcdfiles, "pcd");
    return pcdfiles.size();
};
int YDGetYAMLFilePath(std::string strFileDirectory,
                      std::vector<std::string> &yamlfiles)
{
    readFileList(strFileDirectory, yamlfiles, "yaml");
    return yamlfiles.size();
}
int YDGetIMGFilePath(std::string strFileDirectory,
                     std::vector<std::string> &imgfiles)
{
    readFileList(strFileDirectory, imgfiles, "jpg");
    readFileList(strFileDirectory, imgfiles, "png");
    readFileList(strFileDirectory, imgfiles, "jpeg");
    readFileList(strFileDirectory, imgfiles, "bmp");
    readFileList(strFileDirectory, imgfiles, "JPG");
    return imgfiles.size();
}

void GetImageRect(IplImage *orgImage, CvRect rectInImage, IplImage *imgRect)
{
    CvSize size;
    size.width = rectInImage.width;
    size.height = rectInImage.height;
    cvSetImageROI(orgImage, rectInImage);
    cvCopy(orgImage, imgRect);
    cvResetImageROI(orgImage);
}
void ReadCalibrationFile(std::string _choose_file,
                         cv::Mat &out_RT, cv::Mat &out_intrinsic, cv::Mat &out_dist_coeff, cv::Size &imagesize)
{
    std::cout << "  read from yaml file: " << _choose_file << std::endl;
    cv::FileStorage fs(_choose_file, cv::FileStorage::READ);
    if (!fs.isOpened())
    {
        std::cout << "Cannot open file calibration file" << _choose_file << std::endl;
    }
    else
    {
        fs["CameraMat"] >> out_intrinsic;
        fs["DistCoeff"] >> out_dist_coeff;
        fs["CameraExtrinsicMat"] >> out_RT;
        fs["ImageSize"] >> imagesize;
    }
}
void ReadCalibrationFile_omni(std::string _choose_file,
                              cv::Mat &out_RT, cv::Mat &out_intrinsic, cv::Mat &out_dist_coeff, double& xi,cv::Size &imagesize)
{
    //std::cout << "_choose_file: " << _choose_file << std::endl;
    cv::FileStorage fs(_choose_file, cv::FileStorage::READ);
    if (!fs.isOpened())
    {
        std::cout << "Cannot open file calibration file" << _choose_file << std::endl;
    }
    else
    {
        fs["CameraMat"] >> out_intrinsic;
        fs["DistCoeff"] >> out_dist_coeff;
        fs["CameraExtrinsicMat"] >> out_RT;
        fs["xi"]>>xi;
        fs["ImageSize"] >> imagesize;
    }
}
void saveCalibrationFile(cv::Mat in_extrinsic, cv::Mat in_intrinsic, cv::Mat in_dist_coeff, cv::Size in_size, double reprojectionError, std::string save_path,std::string name)
{
    std::string path_filename_str = save_path +name+"_calibration.yaml";
    cv::FileStorage fs(path_filename_str.c_str(), cv::FileStorage::WRITE);
    if (!fs.isOpened())
    {
        fprintf(stderr, "%s : cannot open file\n", path_filename_str.c_str());
        exit(EXIT_FAILURE);
    }
    in_extrinsic.convertTo(in_extrinsic,CV_64F);
    in_intrinsic.convertTo(in_intrinsic, CV_64F);
    in_dist_coeff.convertTo(in_dist_coeff, CV_64F);
    in_dist_coeff = in_dist_coeff.t();
    fs << "CameraExtrinsicMat" << in_extrinsic;
    fs << "CameraMat" << in_intrinsic;
    fs << "DistCoeff" << in_dist_coeff;
    fs << "ImageSize" << in_size;
    fs << "ReprojectionError" << reprojectionError;
    fs << "DistModel"
       << "plumb_bob";

    printf(" wrote Calibration file in: %s\n", path_filename_str.c_str());
}
void saveCalibrationFile_omni(cv::Mat in_extrinsic, cv::Mat in_intrinsic, cv::Mat in_dist_coeff, double xi,cv::Size in_size, double reprojectionError, std::string save_path,std::string name)
{
    std::string path_filename_str = save_path +name+"_calibration.yaml";
    cv::FileStorage fs(path_filename_str.c_str(), cv::FileStorage::WRITE);
    if (!fs.isOpened())
    {
        fprintf(stderr, "%s : cannot open file\n", path_filename_str.c_str());
        exit(EXIT_FAILURE);
    }
    in_extrinsic.convertTo(in_extrinsic,CV_64F);
    in_intrinsic.convertTo(in_intrinsic, CV_64F);
    in_dist_coeff.convertTo(in_dist_coeff, CV_64F);
    in_dist_coeff = in_dist_coeff.t();
    fs << "CameraExtrinsicMat" << in_extrinsic;
    fs << "CameraMat" << in_intrinsic;
    fs << "DistCoeff" << in_dist_coeff;
    fs << "xi "<< xi;
    fs << "ImageSize" << in_size;
    fs << "ReprojectionError" << reprojectionError;
    fs << "DistModel"
       << "plumb_bob";

    printf(" wrote Calibration file in: %s\n", path_filename_str.c_str());
}
void CutImage(std::string imgpath,cv::Mat &Fimg,cv::Mat &Rimg)
{
    IplImage *src;
    src = cvLoadImage(imgpath.c_str(), 1);
    IplImage *dst;
    CvRect rect = cvRect(0, 0, 0.5 * src->width, src->height);
    CvSize dst_size;
    dst_size.height = rect.height;
    dst_size.width = rect.width;
    dst = cvCreateImage(dst_size, IPL_DEPTH_8U, src->nChannels);
    GetImageRect(src, rect, dst);
    Rimg=cv::cvarrToMat(dst);
    IplImage *dst1;
    CvRect rect1 = cvRect(0.5 * src->width, 0, 0.5 * src->width, src->height);
    CvSize dst_size1;
    dst_size1.height = rect1.height;
    dst_size1.width = rect1.width;
    dst1 = cvCreateImage(dst_size1, IPL_DEPTH_8U, src->nChannels);
    GetImageRect(src, rect1, dst1);
    Fimg=cv::cvarrToMat(dst1);
    //
    cout<<"rect to 2880x2880"<<endl;
    cv::Rect rect2880(80, 80, 2880, 2880);
    Mat image_roi = Fimg(rect2880);
    Fimg=image_roi.clone();

}
float yd_solve_pnp(
        std::string outname
        ,cv::Mat frame
        ,std::vector<cv::Point2d> centers2D
        ,std::vector<cv::Point3d> centers3D
        ,cv::Mat& exRT
        ,cv::Mat _colorprojection_matrix
        ,cv::Mat _distortion_matrix
        ,cv::Size in_size
        ,cv::Mat& _pnp_r
        ,cv::Mat& _pnp_t
        ,cv::Mat& _camparamter
        ,unsigned int solvemod)
{

    cv::Mat Edistor_matrix=cv::Mat::zeros(_distortion_matrix.rows, 1, CV_64F);
    cv::Mat inliers_temp;
    cv::Mat pnp_r_temp = Mat::zeros(1, 3, CV_32F);
    cv::Mat pnp_t_temp = Mat::zeros(1, 3, CV_32F);
    cv::Mat inliers;
    cv::Mat m_r = Mat::zeros(3, 3, CV_32F);
    cv::Mat pnp_r = Mat::zeros(1, 3, CV_32F);
    cv::Mat pnp_t = Mat::zeros(1, 3, CV_32F);
    cv::Mat color_map_exRT=cv::Mat::zeros(4, 4, CV_64F);
    cv::Mat camparamter= Mat::zeros(14, 1, CV_64F);
    if(solvemod==0) {
        bool sucess =cv::solvePnP(centers3D, centers2D, _colorprojection_matrix, Edistor_matrix, pnp_r_temp, pnp_t_temp, true,  1);
        if (sucess)
        {
            inliers = inliers_temp.clone();
            pnp_r = pnp_r_temp.clone();
            pnp_t = pnp_t_temp.clone();
        }
        else
        {
            std::cout<<"  solvePnP failed"<<std::endl;
        }
    }
    if(solvemod==1) {
        float thead = 20.0f;
        for (; thead > 0;) {
            cv::Mat inliers_temp;
            bool sucess = solvePnPRansac(centers3D, centers2D, _colorprojection_matrix, Edistor_matrix, pnp_r_temp,
                                         pnp_t_temp,
                                         false, 3000, thead, 0.995, inliers_temp, SOLVEPNP_EPNP);

            if (sucess) {
                //cout << "when thead=" << thead << ", inliers point num=" << inliers.rows << endl;
                if (thead < 1.6f) {
                    thead = thead - 0.2f;
                } else {
                    thead = thead - 0.5f;
                }
                thead = thead - 0.5f;
                inliers = inliers_temp.clone();
                pnp_r = pnp_r_temp.clone();
                pnp_t = pnp_t_temp.clone();
            } else {
                break;
            }

        }
    }

    cv::Point3f m_t(pnp_t);
    cv::Rodrigues(pnp_r, m_r);
    cv::Mat m_R = m_r.t();
    m_r.copyTo(color_map_exRT(cv::Rect_<double>(0, 0, 3, 3)));
    color_map_exRT.at<double>(0, 3) = m_t.x;
    color_map_exRT.at<double>(1, 3) = m_t.y;
    color_map_exRT.at<double>(2, 3) = m_t.z;
    color_map_exRT.at<double>(3, 3) = 1;
    Eigen::Matrix4f extrinsicstemp;
    cv::cv2eigen(color_map_exRT, extrinsicstemp);
    Eigen::Affine3f temp;
    temp.matrix() = extrinsicstemp;
    extrinsicstemp = temp.inverse().matrix();
    cv::eigen2cv(extrinsicstemp, color_map_exRT);
    color_map_exRT.convertTo(color_map_exRT, CV_64F);
    exRT=color_map_exRT.clone();
    std::vector<cv::Point2d> projectedPoints;
    cv::projectPoints(centers3D, pnp_r, pnp_t, _colorprojection_matrix, Edistor_matrix, projectedPoints);
    float reperror = 0;
    float error;
    for (int id = 0; id < centers3D.size(); id++)
    {
        error = sqrt(pow(centers2D[id].x - projectedPoints[id].x, 2) + pow(centers2D[id].y - projectedPoints[id].y, 2));
        cv::circle(frame, cv::Point2f(projectedPoints[id].x, projectedPoints[id].y), 1, cv::Scalar(255,0, 0), 2, 3, 0);
        cv::circle(frame, cv::Point2f(centers2D[id].x, centers2D[id].y), 1, cv::Scalar(0,255, 0), 2, 3, 0);
        reperror = reperror + error;
    }
    std::cout << " PNP--------reprojection error is :" << reperror / centers3D.size() << std::endl;
    string name=outname+"_projectedPoints.jpg";
    cv::imwrite(name, frame);
    pnp_r.copyTo(camparamter(cv::Rect_<double>(0, 0, 1, 3)));
    pnp_t.copyTo(camparamter(cv::Rect_<double>(0, 3, 1, 3)));
    camparamter.at<double>(6,0)=_colorprojection_matrix.at<double>(0,0);
    camparamter.at<double>(7,0)=_colorprojection_matrix.at<double>(1,1);
    camparamter.at<double>(8,0)=_colorprojection_matrix.at<double>(0,2);
    camparamter.at<double>(9,0)=_colorprojection_matrix.at<double>(1,2);
    camparamter.at<double>(10,0)=_distortion_matrix.at<double>(0,0);
    camparamter.at<double>(11,0)=_distortion_matrix.at<double>(1,0);
    camparamter.at<double>(12,0)=_distortion_matrix.at<double>(2,0);
    camparamter.at<double>(13,0)=_distortion_matrix.at<double>(3,0);
    _camparamter=camparamter.clone();
    float outputerror=reperror / centers3D.size();
    return outputerror;
}
void UniformSamplingfilter_RGB(pcl::PointCloud<pcl::PointXYZRGB> &in_pc,float a,pcl::PointCloud<pcl::PointXYZRGB> &out_pc)
{
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGB>(in_pc));
    pcl::UniformSampling<pcl::PointXYZRGB> filter;
    filter.setInputCloud(cloud);
    filter.setRadiusSearch(a);
    filter.filter(out_pc);
}

void UniformSamplingfilter(pcl::PointCloud<pcl::PointXYZ> &in_pc,float a,pcl::PointCloud<pcl::PointXYZ> &out_pc)
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>(in_pc));
    pcl::UniformSampling<pcl::PointXYZ> filter;
    filter.setInputCloud(cloud);
    filter.setRadiusSearch(a);
    filter.filter(out_pc);
}
void Points_Normal(std::vector<cv::Point2d> inpoints,std::vector<cv::Point2d> &normalpoints,cv::Mat K)
{
    normalpoints.clear();
    Matx33d camMat=K;
    Matx33d camMat_inv = camMat.inv();
    //计算初始外参至camparamter
    for(int m=0;m<inpoints.size();m++)
    {

        Vec3d srcv = Vec3d(inpoints[m].x, inpoints[m].y, 1.0);
        Vec3d dstv = camMat_inv*srcv;
        normalpoints.push_back(cv::Point2f(dstv[0],dstv[1]));
    }
}
void RenderCheck(pcl::PointCloud<pcl::PointXYZRGB> point_cloud,
                 Eigen::Matrix4d transform,
                 std::string outpath,
                 cv::Mat projection_matrix,
                 cv::Mat dis_matrix,
                 cv::Mat image,
                 cv::Rect frame,
                 std::string outname,
                 pcl::PointCloud<pcl::PointXYZRGB> &out_pc)
{
    out_pc.resize(0);
    cv::Mat plane = cv::Mat::zeros(frame.size(), CV_32FC1);
    std::vector<cv::Point3f> obj;
    std::vector<cv::Point2f> uv;
    out_pc.resize(0);
    //plane = image.clone();
    for (auto pt = point_cloud.begin();
         pt < point_cloud.end(); pt++) {
        cv::Point3f temp3f(pt->x, pt->y, pt->z);
        obj.push_back(temp3f);
    }
    cv::Mat rvec = Mat::zeros(1, 3, CV_32F);
    cv::Mat tvec = Mat::zeros(1, 3, CV_32F);
    if (dis_matrix.size[0] == 5||dis_matrix.size[1] == 5)
    {
        cv::projectPoints(obj, rvec, tvec, projection_matrix, dis_matrix,uv);
    }
    else if (dis_matrix.size[0] == 4||dis_matrix.size[1] == 4)
    {
        cv::fisheye::projectPoints(obj, uv, rvec, tvec, projection_matrix, dis_matrix);
    }

    for (int j = 0;j < obj.size(); j++) {
        pcl::PointXYZRGB temp;
        cv::Point xy(uv[j].x, uv[j].y);
        if (xy.inside(frame)&&obj[j].z >0) {
            Vec3b vec_3 = image.at<Vec3b>(xy.y, xy.x);
            temp.x = obj[j].x;
            temp.y = obj[j].y;
            temp.z = obj[j].z;
            temp.r = vec_3[2];
            temp.g = vec_3[1];
            temp.b = vec_3[0];
            out_pc.push_back(temp);

            //cv::circle(plane, xy, 2, cv::Scalar(0, 0, 255), 0, 5, 0);
        }
        else
        {
            out_pc.push_back(point_cloud[j]);
        }
    }
    pcl::PointCloud<pcl::PointXYZRGB> transformed_cloud;
    pcl::transformPointCloud(out_pc, transformed_cloud, transform);
    out_pc.clear();
    pcl::copyPointCloud(transformed_cloud,out_pc);
    //std::string outVprofilename = outpath + outname + "_cVproIMG.jpg";
    std::string outplyfilename = outpath + outname + "_rendermap.ply";
    //cv::imwrite(outVprofilename, plane);
    pcl::io::savePLYFile(outplyfilename, out_pc);
    cout<<" wrote rendercheck file in: "<<outplyfilename<<endl;
}
void RenderCheck_omni(pcl::PointCloud<pcl::PointXYZRGB> point_cloud,
                 Eigen::Matrix4d transform,
                 std::string outpath,
                 cv::Mat projection_matrix,
                 cv::Mat dis_matrix,
                 double xi,
                 cv::Mat image,
                 cv::Rect frame,
                 std::string outname,
                 pcl::PointCloud<pcl::PointXYZRGB> &out_pc)
{
    out_pc.resize(0);
    cv::Mat plane = cv::Mat::zeros(frame.size(), CV_32FC1);
    std::vector<cv::Point3f> obj;
    std::vector<cv::Point2f> uv;
    out_pc.resize(0);
    //plane = image.clone();
    for (auto pt = point_cloud.begin();
         pt < point_cloud.end(); pt++) {
        cv::Point3f temp3f(pt->x, pt->y, pt->z);
        obj.push_back(temp3f);
    }
    cv::Mat rvec = Mat::zeros(1, 3, CV_32F);
    cv::Mat tvec = Mat::zeros(1, 3, CV_32F);
    cv::omnidir::projectPoints(obj, uv, rvec, tvec, projection_matrix, xi,dis_matrix);

    for (int j = 0;j < obj.size(); j++) {
        pcl::PointXYZRGB temp;
        cv::Point xy(uv[j].x, uv[j].y);
        if (xy.inside(frame)&&obj[j].z >-0.2f) {
            Vec3b vec_3 = image.at<Vec3b>(xy.y, xy.x);
            temp.x = obj[j].x;
            temp.y = obj[j].y;
            temp.z = obj[j].z;
            temp.r = vec_3[2];
            temp.g = vec_3[1];
            temp.b = vec_3[0];
            out_pc.push_back(temp);

            //cv::circle(plane, xy, 2, cv::Scalar(0, 0, 255), 0, 5, 0);
        }
//        else
//        {
//            out_pc.push_back(point_cloud[j]);
//        }
    }
    pcl::PointCloud<pcl::PointXYZRGB> transformed_cloud;
    pcl::transformPointCloud(out_pc, transformed_cloud, transform);
    out_pc.clear();
    pcl::copyPointCloud(transformed_cloud,out_pc);
    //std::string outVprofilename = outpath + outname + "_cVproIMG.jpg";
    std::string outplyfilename = outpath + outname + "_rendermap.ply";
    //cv::imwrite(outVprofilename, plane);
    pcl::io::savePLYFile(outplyfilename, out_pc);
    cout<<" wrote rendercheck file in: "<<outplyfilename<<endl;
}
void fardisfilter(pcl::PointCloud<pcl::PointXYZ> &input, pcl::PointCloud<pcl::PointXYZ> &output,float radius)
{
    for(auto pt=input.begin();pt<input.end();pt++)
    {
        if(sqrt(pt->x*pt->x+pt->y*pt->y+pt->z*pt->z)>radius)
            output.push_back(pcl::PointXYZ(pt->x,pt->y,pt->z));
    }
}
void getneighbors(pcl::PointCloud<pcl::PointXYZ> &source, pcl::PointCloud<pcl::PointXYZ> &searchPoint, pcl::PointCloud<pcl::PointXYZ> &neibors)
{
    float resolution = 0.01f;
    pcl::PointCloud<pcl::PointXYZ>::Ptr Sourceptr(new pcl::PointCloud<pcl::PointXYZ>(source));
    pcl::PointCloud<pcl::PointXYZ>::Ptr Searchptr(new pcl::PointCloud<pcl::PointXYZ>(searchPoint));
    pcl::octree::OctreePointCloudSearch<pcl::PointXYZ> octree(resolution);
    octree.setInputCloud(Sourceptr);
    octree.addPointsFromInputCloud();
    float radius = 0.2f;
    std::vector<int> pointIdxRadiusSearch;
    std::vector<float> pointRadiusSquaredDistance;
    for (pcl::PointCloud<pcl::PointXYZ>::iterator pt = searchPoint.begin();
         pt < searchPoint.end(); pt++)
    {
        pointIdxRadiusSearch.resize(0);
        if (octree.radiusSearch(*pt, radius, pointIdxRadiusSearch, pointRadiusSquaredDistance) > 0)
        {
            for (std::size_t i = 0; i < pointIdxRadiusSearch.size(); ++i)
                neibors.push_back(source[pointIdxRadiusSearch[i]]);
        }
    }
//去重
    std::cout<<"  delete repetitive neiborpoints from "<<neibors.size();
    pcl::PointCloud<pcl::PointXYZ> temp;
    pcl::copyPointCloud(neibors,temp);
    neibors.clear();
    pcl::PointCloud<pcl::PointXYZ>::Ptr DownPtr(new pcl::PointCloud<pcl::PointXYZ>(temp));
    pcl::UniformSampling<pcl::PointXYZ> filter;
    filter.setInputCloud(DownPtr);
    filter.setRadiusSearch(0.001f);
    filter.filter(neibors);
    std::cout<<" to "<<neibors.size()<< " ."<<endl;
}

void M4ftoM4d(Eigen::Matrix4f &transformation, Eigen::Matrix4d &trans_d)
{
    trans_d(0, 0) = transformation(0, 0);
    trans_d(0, 1) = transformation(0, 1);
    trans_d(0, 2) = transformation(0, 2);
    trans_d(0, 3) = transformation(0, 3);
    trans_d(1, 0) = transformation(1, 0);
    trans_d(1, 1) = transformation(1, 1);
    trans_d(1, 2) = transformation(1, 2);
    trans_d(1, 3) = transformation(1, 3);
    trans_d(2, 0) = transformation(2, 0);
    trans_d(2, 1) = transformation(2, 1);
    trans_d(2, 2) = transformation(2, 2);
    trans_d(2, 3) = transformation(2, 3);
    trans_d(3, 0) = transformation(3, 0);
    trans_d(3, 1) = transformation(3, 1);
    trans_d(3, 2) = transformation(3, 2);
    trans_d(3, 3) = transformation(3, 3);
}
void PrintEular(cv::Mat in_RT)
{
    double rad = 180.0f / M_PI;
    cv::Mat handin_R = in_RT(cv::Rect(0, 0, 3, 3));
    Eigen::Matrix3d rotation_matrix;
    cv::cv2eigen(handin_R, rotation_matrix);
    Eigen::Vector3d eulerAngle = rotation_matrix.eulerAngles(2, 1, 0);
    std::cout << "  angle (z,y,x): " << std::endl
              << "  (" << eulerAngle(0) * rad << " , " << eulerAngle(1) * rad << " , " << eulerAngle(2) * rad << ")" << std::endl;

}
string int2str(int num)
{
    ostringstream oss;
    if (oss << num)
    {
        string str(oss.str());
        return str;
    }
    else
    {
        cout << "[float2str] - Code error" << endl;
        exit(0);
    }
}
int str2int(string str)
{
    int d;
    stringstream sin(str);
    if (sin >> d)
    {
        return d;
    }
    cout << str << endl;
    cout << "Can not convert a string to int" << endl;
    exit(0);
}
void readposematrix(std::string filename, Eigen::Matrix4d &tf)
{

    double qw, qx, qy, qz, tx, ty, tz;
    double rad = 180.0f / M_PI;
    ifstream calib_File(filename);
    while (!calib_File.eof())
    {
        calib_File >>  tx >> ty >> tz >> qx >> qy >> qz >> qw ;
        //std::cout << "got pose data: " << tx << " " << ty << " " << tz << " "<< qx << " " << qy << " " << qz << " " << qw << " "  << std::endl;
    }
    Eigen::Quaterniond quaternion(qw, qx, qy, qz);
    Eigen::Matrix3d rotation_matrix;
    rotation_matrix = quaternion.toRotationMatrix();
    Eigen::Vector3d eulerAngle = rotation_matrix.eulerAngles(2, 1, 0);
//    std::cout << "angle (z,y,x): " << std::endl
//              << "(" << eulerAngle(0) * rad << " , " << eulerAngle(1) * rad << " , " << eulerAngle(2) * rad << ")" << std::endl;
    Eigen::Matrix<double, 3, 1> T;
    T << tx, ty, tz;
    tf = Eigen::Matrix4d::Identity();
    tf.block(0, 0, 3, 3) = rotation_matrix;
    tf.block(0, 3, 3, 1) = T;
    calib_File.close();
}

#endif