#include <iostream>
#include "axiscalibor.h"
using namespace std;
static void help()
{
    printf( "\nThis is a tool to calib rotate axis by 'tagimgpos_calibr' tool's ouput data.\n"
            "Usage: optimextr_calibr\n"
            "     -inputdatapath <the address of data files>         # include image and intrinsic yaml\n"
            "     -predatapath <the address of data files>         # tagimgpos_calibr products temp data\n"
            "     An Example:\n"
            "     ./rotateaxis_calibr -inputdatapath /home/wxy/study/calib/CalibInMarkRoom/0324 -predatapath ../tagimgpos_calibr_output\n"
            "\n" );

}
//
int main(int argc, char **argv)
{
    std::string outputpath=argv[0];
    outputpath="."+ outputpath+"_output/";
    mkdir(outputpath.c_str(),S_IRWXU);
    std::string inputdatapath;
    std::string predatapath;
    std::string tagslisttxt;
    float tagsize;
    std::string devicetype;
    cv::CommandLineParser parser(argc, argv,"{help||}"
                                            "{@inputdatapath||}{@predatapath||}");
    if (parser.has("help"))
    {
        help();
        return 0;
    }
    inputdatapath = parser.get<std::string>("@inputdatapath");
    predatapath=parser.get<std::string>("@predatapath");
    CalibAxis calibor;

    if(!calibor.Init(inputdatapath,predatapath,outputpath))
        return -1;
    if(!calibor.Calibration())
        return -1;
    return 0;
}