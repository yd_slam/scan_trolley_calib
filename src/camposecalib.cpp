#include "camposecalib.h"
#include "tools.h"
#define NONE       "\e[0m"
#define L_RED      "\e[1;31m"
#define L_GREEN    "\e[1;32m"
#define printlr(format, arg...) do{printf(L_RED format NONE,## arg);}while(0)
#define printlg(format, arg...) do{printf(L_GREEN format NONE,## arg);}while(0)
bool JTCalib::Init(std::string _inputdatapath,std::string _outputdatapath,float _tagsize,std::string _devicetype,std::string _tagslisttxt)
{
    outputdatapath=_outputdatapath;
    datapath=_inputdatapath;
    tagslisttxt=_tagslisttxt;
    tagsize=_tagsize;
    if(_devicetype.compare("d")==0)
        devicetypeid=0;
    else if(_devicetype.compare("s")==0)
        devicetypeid=1;
    else if(_devicetype.compare("o")==0)
        devicetypeid=2;
    else{
        printlr("Input Erorr: the device type is not surpport!\n");
        return false;
    }

    std::string pcdfilepath = datapath + "/pcd";
    std::vector<std::string> yamlfilenames,imgfilenames,pcdfilenames;
    std::string frontyaml,rearyaml,omniyaml;
    yamlfilenames.resize(0);
    imgfilenames.resize(0);
    pcdfilenames.resize(0);
    int yamlfilecnt=YDGetYAMLFilePath(datapath,yamlfilenames);
    if(yamlfilecnt==2&&devicetypeid==0)
    {
        frontyaml=datapath+"/front_fisheye_lidar_calibration.yaml";
        rearyaml=datapath+"/rear_fisheye_lidar_calibration.yaml";
    }
    else if(yamlfilecnt==1&&devicetypeid==1)
    {
        frontyaml=yamlfilenames[0];
    }
    else if(yamlfilecnt==2&&devicetypeid==2)
    {
        frontyaml=datapath+"/front_fisheye_lidar_calibration.yaml";
        omniyaml=datapath+"/omni_lidar_calibration.yaml";
    }
//    else
//    {
//        printlr("Inputfile Erorr: yamlfile sum is wrong!\n");
//        return false;
//    }
    if(YDGetIMGFilePath(datapath,imgfilenames)!=1)
    {
        printlr("Inputfile Erorr: image sum is wrong!\n");
        return false;
    }
    if(YDGetPCDDataFilePath(pcdfilepath, pcdfilenames)==0)
    {
        printlr("Inputfile Erorr: velodyne pcd sum is wrong!\n");
        return false;
    }
    for (int i = 0; i < (pcdfilenames.size()>15?15:pcdfilenames.size());i++)
    {
        if (pcl::io::loadPCDFile(pcdfilenames[i], pc_temp) == -1)
        {
            PCL_ERROR("Couldn't read Velodyne pcd files\n");
            continue;
        }
        lidar_frame.insert(lidar_frame.end(), pc_temp.begin(), pc_temp.end());
    }
    std::cout<<"  Loading map.ply ... ..."<<std::endl;
    if (pcl::io::loadPLYFile("../Map/map.ply", color_mapall) == -1)
    {
        printlr("Inputfile Erorr: load map from ../Map failed!\n");
        return false;
    }
    if(ReadMapTagInfo(tagslisttxt,maptaginfos)==0)
    {
        printlr("Inputfile Erorr: map tags info list is none!\n");
        return false;
    }
    UniformSamplingfilter_RGB(color_mapall,0.05,color_map);
    pcl::copyPointCloud(color_map,map);
    if(devicetypeid==0) {
        CutImage(imgfilenames[0], Frontimg, Rearimg);
        cv::imwrite(outputdatapath+"frontfisheye.jpg", Frontimg);
        cv::imwrite(outputdatapath+"rearfisheye.jpg", Rearimg);
        undistor_Frontimg = Frontimg.clone();
        undistor_Rearimg = Rearimg.clone();
        ReadCalibrationFile(frontyaml, front_lidar_exRT, frontproject, frontdistortion, imagesize);
        ReadCalibrationFile(rearyaml, rear_lidar_exRT, rearproject, reardistortion, imagesize);
        cv::fisheye::undistortImage(Frontimg, undistor_Frontimg, frontproject, frontdistortion, frontproject);
        cv::fisheye::undistortImage(Rearimg, undistor_Rearimg, rearproject, reardistortion, rearproject);
        cv::imwrite(outputdatapath+"front_undistorimg.jpg", undistor_Frontimg);
        cv::imwrite(outputdatapath+"rear_undistorimg.jpg", undistor_Rearimg);
    }
    else if(devicetypeid==1)
    {
        Frontimg = cv::imread(imgfilenames[0], 1);
        undistor_Frontimg = Frontimg.clone();
        ReadCalibrationFile(frontyaml, front_lidar_exRT, frontproject, frontdistortion, imagesize);
        if (frontdistortion.size[0] == 5||frontdistortion.size[1] == 5)
        {
            cv::undistort(Frontimg, undistor_Frontimg, frontproject, frontdistortion, frontproject);
            cv::imwrite(outputdatapath+"front_undistorimg.jpg", undistor_Frontimg);
        }
        else if (frontdistortion.size[0] == 4||frontdistortion.size[1] == 4)
        {
            cv::fisheye::undistortImage(Frontimg, undistor_Frontimg, frontproject, frontdistortion, frontproject);
            cv::imwrite(outputdatapath+"front_undistorimg.jpg", undistor_Frontimg);
        }
    }
    else if(devicetypeid==2)
    {
        CutImage(imgfilenames[0], Frontimg, Rearimg);
        Omniimg=Frontimg.clone();
        cv::imwrite(outputdatapath+"frontfisheye.jpg", Frontimg);
        cv::imwrite(outputdatapath+"omnidirection.jpg", Omniimg);
        undistor_Frontimg = Frontimg.clone();
        undistor_Omni=Omniimg.clone();
        ReadCalibrationFile(frontyaml, front_lidar_exRT, frontproject, frontdistortion, imagesize);
        ReadCalibrationFile_omni(omniyaml,omni_lidar_exRT,omniproject,omnidistortion,xi,imagesize);
        cv::fisheye::undistortImage(Frontimg, undistor_Frontimg, frontproject, frontdistortion, frontproject);
        cv::omnidir::undistortImage(Omniimg, undistor_Omni, omniproject, omnidistortion, xi, cv::omnidir::CALIB_USE_GUESS, omniproject, imagesize);
        cv::imwrite(outputdatapath+"front_undistorimg.jpg", undistor_Frontimg);
        cv::imwrite(outputdatapath+"omni_undistorimg.jpg", undistor_Omni);

    }
    printlg("Init successed!\n");
    return true;
}
int JTCalib::ReadMapTagInfo(std::string txtfilename,std::vector<taginfo3D> &_3dtags)
{
    ifstream _File(txtfilename);
    double x,y,z;
    int id;
    int n=0;
    while (!_File.eof())
    {
        taginfo3D temp;
        _File >>id >> x >> y>> z  ;
        temp.id=id;
        temp.centerpose=cv::Point3d(x,y,z);
        _3dtags.push_back(temp);
        n++;
    }
    _File.close();
    return n;

}
bool JTCalib::DetectTool()
{
    DetectTags(undistor_Frontimg,frontproject,Ftags,"front");
    printlg("%d tags detected\n",Ftags.size());
    if(devicetypeid==0)
    {
        DetectTags(undistor_Rearimg,rearproject,Rtags,"rear");
        printlg("%d tags detected\n",Rtags.size());
        return ((Ftags.size()>3)&&(Rtags.size()>3));
    }
    if(devicetypeid==2)
    {
        DetectTags(undistor_Omni,omniproject,Omnitags,"omni");
        printlg("%d tags detected in omni\n",Omnitags.size());
        return ((Ftags.size()>3)&&(Omnitags.size()>3));
    }
    return Ftags.size()>3;
}
bool JTCalib::DetectTags(cv::Mat frame,
                         cv::Mat projection_matrix,
                         std::vector<taginfo2D> &_tags,
                         std::string outname)
{
    printlg("Detecting AprilTags in %s image...\n",outname.c_str());
    //init camera
    apriltag_detection_info_t info;
    info.tagsize = tagsize; //打印的qrcode尺寸大小(m)
    info.fx = projection_matrix.at<double>(0, 0);
    info.fy = projection_matrix.at<double>(1, 1);
    info.cx = projection_matrix.at<double>(0, 2);
    info.cy = projection_matrix.at<double>(1, 2);
    cv::Mat gray;
    cvtColor(frame, gray, cv::COLOR_BGR2GRAY);
    // Initialize tag detector with options
    apriltag_family_t *tf = NULL;
    tf = tag36h11_create();
    apriltag_detector_t *td = apriltag_detector_create();
    apriltag_detector_add_family(td, tf);
    // Make an image_u8_t header for the Mat data
    image_u8_t im = {.width = gray.cols,
            .height = gray.rows,
            .stride = gray.cols,
            .buf = gray.data};
    zarray_t *detections = apriltag_detector_detect(td, &im);
    std::cout << zarray_size(detections) << " tags detected" << std::endl;
    if (zarray_size(detections) == 0)
    {
        return false;
    }
    for (int i = 0; i < zarray_size(detections); i++)
    {
        taginfo2D infotemp;
        apriltag_detection_t *det;
        cv::Mat extrinsics = cv::Mat::eye(4, 4, CV_64F);
        zarray_get(detections, i, &det);
        info.det = det; //将det赋给info用于计算
        apriltag_pose_t pose;
        double err = estimate_tag_pose(&info, &pose);
        std::cout << "  Tag id: " << det->id << " error " << err << std::endl;
        infotemp.id = det->id;
        infotemp.centerpose = cv::Point2d(det->c[0], det->c[1]);
        infotemp.error = err;
        _tags.push_back(infotemp);

        //row-major order行优先
        extrinsics.at<double>(0, 0) = pose.R->data[0];
        extrinsics.at<double>(0, 1) = pose.R->data[1];
        extrinsics.at<double>(0, 2) = pose.R->data[2];
        extrinsics.at<double>(1, 0) = pose.R->data[3];
        extrinsics.at<double>(1, 1) = pose.R->data[4];
        extrinsics.at<double>(1, 2) = pose.R->data[5];
        extrinsics.at<double>(2, 0) = pose.R->data[6];
        extrinsics.at<double>(2, 1) = pose.R->data[7];
        extrinsics.at<double>(2, 2) = pose.R->data[8];
        extrinsics.at<double>(0, 3) = pose.t->data[0];
        extrinsics.at<double>(1, 3) = pose.t->data[1];
        extrinsics.at<double>(2, 3) = pose.t->data[2];
        extrinsics.at<double>(3, 3) = 1;
        // std::cout << "RT: " << std::endl
        //           << extrinsics << std::endl; //Tag相对于摄像头的位姿
        //plane
        Eigen::Vector4d vec;
        float d = (pose.t->data[0]) * (pose.R->data[2]) + (pose.t->data[1]) * (pose.R->data[5]) + (pose.t->data[2]) * (pose.R->data[8]);
        vec << pose.R->data[2], pose.R->data[5], pose.R->data[8], -d;
//        std::cout << "Plane abcd: " << std::endl
//                  << vec(0) << "," << vec(1) << "," << vec(2) << "," << vec(3) << std::endl;
        //Planes.push_back(vec);

        line(frame, cv::Point(det->p[0][0], det->p[0][1]),
             cv::Point(det->p[1][0], det->p[1][1]),
             cv::Scalar(0, 0xff, 0), 2);
        line(frame, cv::Point(det->p[0][0], det->p[0][1]),
             cv::Point(det->p[3][0], det->p[3][1]),
             cv::Scalar(0, 0, 0xff), 2);
        line(frame, cv::Point(det->p[1][0], det->p[1][1]),
             cv::Point(det->p[2][0], det->p[2][1]),
             cv::Scalar(0xff, 0, 0), 2);
        line(frame, cv::Point(det->p[2][0], det->p[2][1]),
             cv::Point(det->p[3][0], det->p[3][1]),
             cv::Scalar(0xff, 0, 0), 2);

        std::stringstream ss;
        ss << det->id;
        cv::String text = ss.str();
        int fontface = cv::FONT_HERSHEY_SCRIPT_SIMPLEX;
        double fontscale = 1.0;
        int baseline;
        cv::Size textsize = getTextSize(text, fontface, fontscale, 2,
                                        &baseline);
        putText(frame, text, cv::Point(det->c[0] - textsize.width / 2, det->c[1] + textsize.height / 2),
                fontface, fontscale, cv::Scalar(255, 0, 255), 2);
        cv::circle(frame, cv::Point2f(det->c[0], det->c[1]), 1, cv::Scalar(0, 0 , 255), 2, 3, 0);
    }
    apriltag_detections_destroy(detections);
    string name=outputdatapath+outname+"_TagDetections.jpg";
    cv::imwrite(name, frame);
    apriltag_detector_destroy(td);
    tag36h11_destroy(tf);
    return true;
}
int JTCalib::ResoreKeypPoints(std::vector<taginfo2D> tags,std::vector<cv::Point2d> &centers2D,std::vector<cv::Point3d> &centers3D,std::string name)
{
    int n=0;
    ofstream keysfile;
    keysfile.open(outputdatapath+name+"_keys2d3d.txt");
    for(int i=0;i<tags.size();i++)
    {
        int id=tags[i].id;
        for(int j=0;j<maptaginfos.size();j++)
        {
            if(id==maptaginfos[j].id)
            {
                centers2D.push_back(tags[i].centerpose);
                centers3D.push_back(maptaginfos[j].centerpose);
                keysfile << tags[i].centerpose.x<< ' ' << tags[i].centerpose.y << " "<<
                         maptaginfos[j].centerpose.x<<" "<<maptaginfos[j].centerpose.y<<" "<<maptaginfos[j].centerpose.z<<endl;
                n++;
            }
        }
    }
    keysfile.close();
    return n;
}
bool JTCalib::Calibration()
{
    std::vector<cv::Point2d> f_centers2D,r_centers2D,o_centers2D;
    std::vector<cv::Point3d> f_centers3D,r_centers3D,o_centers3D;
    double initerror=999;

    if(devicetypeid==0)
        if((ResoreKeypPoints(Rtags,r_centers2D,r_centers3D,"rear")<4||ResoreKeypPoints(Ftags,f_centers2D,f_centers3D,"front")<4))
        {
            printlr("Detect Erorr:  registration in map less than 5, calibration is off!\n");
            return false;
        }
    if(devicetypeid==1)
        if(ResoreKeypPoints(Ftags,f_centers2D,f_centers3D,"front")<4)
        {
            printlr("Detect Erorr:  registration in map less than 5, calibration is off!\n");
            return false;
        }
    if(devicetypeid==2)
        if((ResoreKeypPoints(Omnitags,o_centers2D,o_centers3D,"omni")<4||ResoreKeypPoints(Ftags,f_centers2D,f_centers3D,"front")<4))
        {
            printlr("Detect Erorr:  registration in map less than 5, calibration is off!\n");
            return false;
        }

    double error;
//    std::vector<cv::Point2d> discenters2D,centers2D_normal;
//    Points_Normal(f_centers2D,centers2D_normal,frontproject);
//    cv::fisheye::distortPoints(centers2D_normal,discenters2D,frontproject, frontdistortion);
//
//    for(int w=0;w<discenters2D.size();w++)
//    {
//        //cv::circle(Frontimg, discenters2D[w], 1, cv::Scalar(0, 0 , 255), 2, 3, 0);
//
//    }
//    cv::imwrite("../temp/test.jpg", Frontimg);

    error = yd_solve_pnp(outputdatapath + "front", undistor_Frontimg, f_centers2D, f_centers3D, front_map_exRT,
                         frontproject, frontdistortion, imagesize, pnp_r,
                         pnp_t, camparamter, 1);

//    TestK1k2K3K4(front_map_exRT, frontproject, frontdistortion,discenters2D, f_centers3D, Frontimg);

//    while(initerror-error>0.1f) {
//        initerror = error;

//        bundle_adjustment(camparamter, discenters2D, f_centers3D, frontproject, frontdistortion);
//        cv::Mat imggg;
//        cv::fisheye::undistortImage(Frontimg,undistor_Frontimg,frontproject, frontdistortion,frontproject);
//        cv::imwrite("../temp/new.jpg", undistor_Frontimg);
//    return false;
////        //优化得到新的内参和畸变

//        Ftags.clear();
//        f_centers2D.clear();
//        f_centers3D.clear();
//        cout<<"4"<<endl;
//        DetectTags(undistor_Frontimg,frontproject,Ftags,"front");
//        //获得新的无畸变2d-3d点对
//        cout<<"5"<<endl;
//        ResoreKeypPoints(Ftags,f_centers2D,f_centers3D,"front");
//        //重新计算新的外参
//        cout<<"6"<<endl;
//        error = yd_solve_pnp(outputdatapath + "front", undistor_Frontimg, f_centers2D, f_centers3D, front_map_exRT,
//                             frontproject, frontdistortion, imagesize, pnp_r,
//                             pnp_t, camparamter, 1);

//    }
//
//    return false;
    saveCalibrationFile(front_map_exRT, frontproject, frontdistortion, imagesize, error, outputdatapath,"front_map");
    if(devicetypeid==0) {
//        Points_Normal(r_centers2D,centers2D_normal,rearproject);
//        discenters2D.clear();
//        cv::fisheye::distortPoints(centers2D_normal,discenters2D,rearproject, reardistortion);
        error = yd_solve_pnp(outputdatapath+"rear",undistor_Rearimg,r_centers2D, r_centers3D, rear_map_exRT,
                             rearproject, reardistortion, imagesize, pnp_r,
                             pnp_t, camparamter, 1);
//        bundle_adjustment(camparamter, discenters2D, r_centers3D, rearproject, reardistortion);
//                cv::Mat imggg;
//        cv::fisheye::undistortImage(Rearimg,imggg,rearproject, reardistortion,rearproject);
//        cv::imwrite("../temp/new.jpg", imggg);
        saveCalibrationFile(rear_map_exRT, rearproject, reardistortion, imagesize, error, outputdatapath, "rear_map");
    }
    if(devicetypeid==2)
    {
        error = yd_solve_pnp(outputdatapath+"omni",undistor_Omni,o_centers2D, o_centers3D, omni_map_exRT,
                             omniproject, omnidistortion, imagesize, pnp_r,
                             pnp_t, camparamter, 1);
        saveCalibrationFile_omni(omni_map_exRT, omniproject, omnidistortion, xi,imagesize, error, outputdatapath, "omni_map");
    }
    return true;

}
void JTCalib::TestK1k2K3K4(cv::Mat cam_map_exRT, cv::Mat project, cv::Mat distortion,std::vector<cv::Point2d> discenters2D,std::vector<cv::Point3d> centers3D,cv::Mat image)
{
    cv::Mat plane=image.clone();

    cv::Mat map_color_exRT = Mat::zeros(4, 4, CV_32F);
    Eigen::Matrix4d extrinsicstemp;
    Eigen::Affine3d temp;
    cv::cv2eigen(cam_map_exRT, extrinsicstemp);
    temp.matrix() = extrinsicstemp;
    extrinsicstemp = temp.inverse().matrix();
    double fx=project.at<double>(0,0);
    double fy=project.at<double>(1,1);
    double cx=project.at<double>(0,2);
    double cy=project.at<double>(1,2);
    double k1 = distortion.at<double>(0, 0);
    double k2= distortion.at<double>(1, 0);
    double k3 = distortion.at<double>(2, 0);
    double k4 = distortion.at<double>(3, 0);
    cout<<fx<<" "<<fy<<" "<<cx<<" "<<cy<<" "<<k1<<" "<<k2<<" "<<k3<<" "<<k4<<endl;
    pcl::PointCloud<pcl::PointXYZ> centers3Dinworld,centers3Dincam;
    for(int i=0;i<centers3D.size();i++)
        centers3Dinworld.push_back(pcl::PointXYZ(centers3D[i].x,centers3D[i].y,centers3D[i].z));
    pcl::transformPointCloud(centers3Dinworld,centers3Dincam,extrinsicstemp);
    std::vector<cv::Point2d> pt2d,pt2dnormal,dispt2d;
    for(int j=0;j<centers3D.size();j++)
    {
        double xp = centers3Dincam[j].x * fx / centers3Dincam[j].z+cx;
        double yp = centers3Dincam[j].y * fy / centers3Dincam[j].z+cy;
        pt2d.push_back(cv::Point2d(xp,yp));
    }

    Points_Normal(pt2d,pt2dnormal,project);
    cv::fisheye::distortPoints(pt2dnormal,dispt2d,project, distortion);
    for(int j=0;j<centers3D.size();j++)
    {
        cv::circle(plane, dispt2d[j], 1, cv::Scalar(255, 0 ,0), 2, 3, 0);
        cv::circle(plane, discenters2D[j], 1, cv::Scalar( 0 ,255,0), 2, 3, 0);
    }
    cv::imwrite("../temp/K1k2K3K4test.jpg", plane);

}
//void JTCalib::bundle_adjustment(
//        cv::Mat& camparamter,
//        std::vector<cv::Point2d> discenters2D,
//        std::vector<cv::Point3d> centers3D,
//        cv::Mat& _colorprojection_matrix,
//        cv::Mat& _distortion_matrix
//) {
//    double cere_r[3], cere_t[3], cere_fuv[4], k1k2p1p2[4];
//    cere_r[0] = camparamter.at<double>(0, 0);
//    cere_r[1] = camparamter.at<double>(1, 0);
//    cere_r[2] = camparamter.at<double>(2, 0);
//    cere_t[0] = camparamter.at<double>(3, 0);
//    cere_t[1] = camparamter.at<double>(4, 0);
//    cere_t[2] = camparamter.at<double>(5, 0);
//    cere_fuv[0] = camparamter.at<double>(6, 0);
//    cere_fuv[1] = camparamter.at<double>(7, 0);
//    cere_fuv[2] = camparamter.at<double>(8, 0);
//    cere_fuv[3] = camparamter.at<double>(9, 0);
//    k1k2p1p2[0] = camparamter.at<double>(10, 0);
//    k1k2p1p2[1] = camparamter.at<double>(11, 0);
//    k1k2p1p2[2] = camparamter.at<double>(12, 0);
//    k1k2p1p2[3] = camparamter.at<double>(13, 0);
//    cout<<k1k2p1p2[0]<<" "<<k1k2p1p2[1]<<" "<<k1k2p1p2[2]<<" "<<k1k2p1p2[3]<<endl;
//    ceres::Problem problem;
//    // load points
//    //ceres::LossFunction *loss_function = new ceres::HuberLoss(4);    // loss function make bundle adjustment robuster.
//    ceres::LossFunction* loss_function = new ceres::CauchyLoss(5);
//    int num = discenters2D.size();
//    for (int i = 0; i < num; i++) {
//        cv::Point2d observed2d = cv::Point2d(discenters2D[i].x, discenters2D[i].y);
//        cv::Point3d observed3d = cv::Point3d(centers3D[i].x, centers3D[i].y, centers3D[i].z);
//        ceres::CostFunction *cost_function = new ceres::AutoDiffCostFunction<BA_ReprojectCost, 2, 4>
//                (new BA_ReprojectCost(observed2d, observed3d,camparamter));
//        problem.AddResidualBlock(
//                cost_function,
//                loss_function,
//                k1k2p1p2);
//    }
//    // Solve BA
//    ceres::Solver::Options ceres_config_options;
//    ceres_config_options.minimizer_progress_to_stdout = true;
//    ceres_config_options.logging_type = ceres::SILENT;
//    ceres_config_options.num_threads = 3;
//    ceres_config_options.preconditioner_type = ceres::JACOBI;
//    //ceres_config_options.linear_solver_type = ceres::SPARSE_NORMAL_CHOLESKY;
//    ceres_config_options.linear_solver_type = ceres::SPARSE_SCHUR;
//    ceres_config_options.sparse_linear_algebra_library_type = ceres::SUITE_SPARSE;
//
//    ceres::Solver::Summary summary;
//    ceres::Solve(ceres_config_options, &problem, &summary);
//    cout<<k1k2p1p2[0]<<" "<<k1k2p1p2[1]<<" "<<k1k2p1p2[2]<<" "<<k1k2p1p2[3]<<endl;
//    if (!summary.IsSolutionUsable()) {
//        std::cout << "Bundle Adjustment failed." << std::endl;
//    } else {
//
//        //Display statistics about the minimization
//        std::cout << std::endl
//                  << "Bundle Adjustment statistics (approximated RMSE):\n"
//                  << " #residuals: " << summary.num_residuals << "\n"
//                  << " Initial RMSE: " << std::sqrt(summary.initial_cost / summary.num_residuals) << "\n"
//                  << " Final RMSE: " << std::sqrt(summary.final_cost / summary.num_residuals) << "\n"
//                  << " Time (s): " << summary.total_time_in_seconds << "\n";
//
//        cv::Mat cam_3d = (Mat_<double>(3, 1) << cere_r[0], cere_r[1], cere_r[2]);
//        cv::Mat cam_9d;
//        cv::Rodrigues(cam_3d, cam_9d);
//        cv::Mat color_map_exRT = cv::Mat::zeros(4, 4, CV_64F);
//        cv::Mat colorprojection_matrix = cv::Mat::zeros(3, 3, CV_64F);
//        cv::Mat distortion_matrix = Mat::zeros(4, 1, CV_64F);
//        cam_9d.copyTo(color_map_exRT(cv::Rect_<double>(0, 0, 3, 3)));
//        color_map_exRT.at<double>(0, 3) = cere_t[0];
//        color_map_exRT.at<double>(1, 3) = cere_t[1];
//        color_map_exRT.at<double>(2, 3) = cere_t[2];
//        color_map_exRT.at<double>(3, 3) = 1;
//        colorprojection_matrix.at<double>(0, 0) = cere_fuv[0];
//        colorprojection_matrix.at<double>(1, 1) = cere_fuv[1];
//        colorprojection_matrix.at<double>(0, 2) = cere_fuv[2];
//        colorprojection_matrix.at<double>(1, 2) = cere_fuv[3];
//        colorprojection_matrix.at<double>(2, 2) = (double) 1.0f;
//        distortion_matrix.at<double>(0, 0) = k1k2p1p2[0];
//        distortion_matrix.at<double>(1, 0) = k1k2p1p2[1];
//        distortion_matrix.at<double>(2, 0) = k1k2p1p2[2];
//        distortion_matrix.at<double>(3, 0) = k1k2p1p2[3];
//        _colorprojection_matrix = colorprojection_matrix.clone();
//        _distortion_matrix = distortion_matrix.clone();
//    }
//}
void JTCalib::Render()
{
    Eigen::Matrix4d transform,transform_inv;
    Eigen::Affine3d temp;
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr transformed_cloud(new pcl::PointCloud<pcl::PointXYZRGB>());
    pcl::PointCloud<pcl::PointXYZRGB> temprgbpc;
    cv::cv2eigen(front_map_exRT, transform);
    temp.matrix() = transform;
    transform_inv = temp.inverse().matrix();
    pcl::transformPointCloud(color_mapall, *transformed_cloud, transform_inv);
    RenderCheck(*transformed_cloud,transform,outputdatapath,frontproject, frontdistortion,Frontimg, cv::Rect(0, 0, imagesize.width, imagesize.height),"front",temprgbpc);
    if(devicetypeid==0) {
        cv::cv2eigen(rear_map_exRT, transform);
        temp.matrix() = transform;
        transform_inv = temp.inverse().matrix();
        pcl::transformPointCloud(temprgbpc, *transformed_cloud, transform_inv);
        RenderCheck(*transformed_cloud, transform,outputdatapath,rearproject, reardistortion,Rearimg,
                    cv::Rect(0, 0, imagesize.width, imagesize.height), "rear",temprgbpc);
    }
    if(devicetypeid==2) {
        cv::cv2eigen(omni_map_exRT, transform);
        temp.matrix() = transform;
        transform_inv = temp.inverse().matrix();
        pcl::transformPointCloud(color_mapall, *transformed_cloud, transform_inv);
        RenderCheck_omni(*transformed_cloud, transform,outputdatapath,omniproject, omnidistortion,xi,Omniimg,
                    cv::Rect(0, 0, imagesize.width, imagesize.height), "omni",temprgbpc);
    }
}