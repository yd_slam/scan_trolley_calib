#ifndef CAMPOSECALIB_H
#define CAMPOSECALIB_H
#include "opencv2/opencv.hpp"
#include "apriltag/apriltag.h"
#include "apriltag/tag36h11.h"
#include "apriltag/common/getopt.h"
#include "apriltag/common/homography.h"
#include "apriltag/apriltag_pose.h"
#include <cmath>
#include "Eigen/Dense"
#include "Eigen/LU"
#include "Eigen/Core"
#include "eigen3/Eigen/Dense"
#include "opencv2/core/eigen.hpp"
#include <opencv2/core/utility.hpp>
#include "opencv2/ccalib/multicalib.hpp"
#include <dirent.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl/point_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include "pcl/features/normal_3d.h"
#include <pcl/filters/frustum_culling.h>
#include <pcl/sample_consensus/ransac.h>
#include <pcl/sample_consensus/sac_model_plane.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/filters/project_inliers.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/surface/mls.h>
//Ceres
#include <iostream>
#include <opencv2/core/core.hpp>
#include <ceres/ceres.h>
#include <ceres/rotation.h>
#include <chrono>
using namespace std;
using namespace pcl;
using namespace cv;

struct taginfo2D{
    int id;
    cv::Point2d centerpose;
    double error;
};
struct taginfo3D{
    int id;
    cv::Point3d centerpose;
};
class JTCalib
{
public:
    bool Init(std::string _inputdatapath,std::string _outputdatapath,float _tagsize,std::string _devicetype,std::string _tagslisttxt);
    bool DetectTool();
    bool Calibration();
    void Render();
private:
    bool DetectTags(cv::Mat frame,cv::Mat projection_matrix,std::vector<taginfo2D> &_tags,std::string outname);
    int ReadMapTagInfo(std::string txtfilename,std::vector<taginfo3D> &_3dtags);
    int ResoreKeypPoints(std::vector<taginfo2D> tags,std::vector<cv::Point2d> &centers2D,std::vector<cv::Point3d> &centers3D,std::string _name);
    void TestK1k2K3K4( cv::Mat cam_map_exRT, cv::Mat project, cv::Mat distortion,std::vector<cv::Point2d> discenters2D,std::vector<cv::Point3d> centers3D,cv::Mat image);
private:
    std::string datapath,tagslisttxt,outputdatapath;
    float tagsize;
    int devicetypeid=0;
    pcl::PointCloud<pcl::PointXYZ> pc_temp, lidar_frame,map;
    pcl::PointCloud<pcl::PointXYZRGB> color_mapall,color_map;
    cv::Mat Frontimg,Rearimg,Omniimg;
    cv::Mat undistor_Frontimg,undistor_Rearimg,undistor_Omni;
    double xi;
    cv::Mat rearproject,frontproject,omniproject=cv::Mat::eye(3, 3, CV_64F);
    cv::Mat rear_lidar_exRT,front_lidar_exRT,omni_lidar_exRT=cv::Mat::eye(4, 4, CV_64F);
    cv::Mat rear_map_exRT,front_map_exRT,omni_map_exRT=cv::Mat::eye(4, 4, CV_64F);
    cv::Mat reardistortion,frontdistortion,omnidistortion,Edistor_matrix=cv::Mat::zeros(4, 1, CV_64F);
    cv::Mat pnp_r = Mat::zeros(1, 3, CV_64F);
    cv::Mat pnp_t = Mat::zeros(1, 3, CV_64F);
    cv::Mat camparamter= Mat::zeros(14, 1, CV_64F);
    cv::Size imagesize;
    std::vector<taginfo2D> Ftags,Rtags,Omnitags;
    std::vector<taginfo3D> maptaginfos;
//    void bundle_adjustment(
//            cv::Mat& camparamter,
//            std::vector<cv::Point2d> discenters2D,
//            std::vector<cv::Point3d> centers3D,
//            cv::Mat& _colorprojection_matrix,
//            cv::Mat& _distortion_matrix
//    );
//private:
//    //BA代价函数
//    struct BA_ReprojectCost
//    {
//        cv::Point2d discenters2D;
//        cv::Point3d centers3D;
//        cv::Mat camparamter;
//        BA_ReprojectCost(cv::Point2d& discenters2D,cv::Point3d& centers3D,cv::Mat &camparamter):
//                discenters2D(discenters2D),centers3D(centers3D),camparamter(camparamter)
//        {}
//        template <typename T>
//        bool operator()(const T* const k1k2k3k4,
//                        T* residuals) const
//        //camparamter:0~2三轴旋转，3~5三轴平移，6~7焦距fxfy，8~9光心cxcy，10~13畸变k1k2p1p2，一共14维
//        {
//            const T cere_r[3]={T(camparamter.at<double>(0, 0)),T(camparamter.at<double>(1, 0)),T(camparamter.at<double>(2, 0))};
//            T pos_proj[3];
//            T pos3d[3];
//            pos3d[0]=T(centers3D.x);
//            pos3d[1]=T(centers3D.y);
//            pos3d[2]=T(centers3D.z);
//            ceres::AngleAxisRotatePoint(cere_r, pos3d, pos_proj);
//            // Apply the camera translation
//            pos_proj[0] += T(camparamter.at<double>(3, 0));
//            pos_proj[1] += T(camparamter.at<double>(4, 0));
//            pos_proj[2] += T(camparamter.at<double>(5, 0));
//            const T fx = T(camparamter.at<double>(6, 0));
//            const T fy = T(camparamter.at<double>(7, 0));
//            const T cx = T(camparamter.at<double>(8, 0));
//            const T cy = T(camparamter.at<double>(9, 0));
//            const T k1 = k1k2k3k4[0];
//            const T k2 = k1k2k3k4[1];
//            const T k3 = k1k2k3k4[2];
//            const T k4 = k1k2k3k4[3];
//            //得到像平面坐标下的理想点，原点在驻点
//            const T xp = pos_proj[0] / pos_proj[2];
//            const T yp = pos_proj[1] / pos_proj[2];
//            const T xd=(T(discenters2D.x)-cx);
//            const T yd=(T(discenters2D.y)-cy);
//            //const T r2=xd*xd+yd*yd;
//            const T r2=xp*xp+yp*yp;
//            residuals[0]=abs((T(1.0)+k1*r2+k2*r2*r2+k3*r2*r2*r2+k4*r2*r2*r2*r2)*xp*fx-xd);
//            residuals[1]=abs((T(1.0)+k1*r2+k2*r2*r2+k3*r2*r2*r2+k4*r2*r2*r2*r2)*yp*fy-yd);
//            return true;
//
//        }
//
//    };



};


#endif
