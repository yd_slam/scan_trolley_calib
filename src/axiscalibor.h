#ifndef AXISCALIBOR_H
#define AXISCALIBOR_H
#include "opencv2/opencv.hpp"
#include <cmath>
#include "Eigen/Dense"
#include "Eigen/LU"
#include "Eigen/Core"
#include "eigen3/Eigen/Dense"
#include "opencv2/core/eigen.hpp"
#include <opencv2/core/utility.hpp>
#include <dirent.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl/point_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include "pcl/features/normal_3d.h"
#include <pcl/filters/frustum_culling.h>
#include <pcl/sample_consensus/ransac.h>
#include <pcl/sample_consensus/sac_model_plane.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/filters/project_inliers.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/surface/mls.h>
//g2o
#include <g2o/core/base_vertex.h>
#include <g2o/core/base_binary_edge.h>
#include <g2o/types/slam3d/vertex_se3.h>
#include <g2o/core/block_solver.h>
#include <g2o/core/optimization_algorithm_levenberg.h>
#include <g2o/solvers/dense/linear_solver_dense.h>
using namespace std;
using namespace pcl;
using namespace cv;

class CalibAxis
{
public:
    bool Init(std::string _datapath,std::string _predatapath,std::string _outputdatapath);
    bool Calibration();
//    bool Optimization();
//    void Render();
private:
    struct DataInfo
    {
        float H;
        float V;
        cv::Mat frame_map_exRT=cv::Mat::zeros(4, 4, CV_64F);
        Eigen::Matrix4d lidar_map_pose;
        cv::Mat frame_lidar_exRT=cv::Mat::zeros(4, 4, CV_64F);
    };
    std::vector<DataInfo> DataInfo2H,DataInfo2V,DataInfoC;
    bool CollectInfo(std::string _datapath,std::vector<std::string> _anglefiles,
                                std::vector<DataInfo> &_DataInfoC,std::vector<DataInfo> &_DataInfo2H,std::vector<DataInfo> &_DataInfo2V);
    bool CalibEveryExRT(DataInfo &inputdatainfo);
    double NDT_Process(pcl::PointCloud<pcl::PointXYZ> &Input, pcl::PointCloud<pcl::PointXYZ> &Target, Eigen::Matrix4d &trans_d);
    void  CalibNewcam_lidar(cv::Mat cam_map_exRT,cv::Mat lidar_map_ndt,cv::Mat lidar_map_exRT,cv::Mat &cam_lidar_exRT);
private:
    std::string datapath,outputdatapath,predatapath,ir_pinhole_yaml;
    pcl::PointCloud<pcl::PointXYZ> pc_temp, lidar_frame,map;
    pcl::PointCloud<pcl::PointXYZRGB> color_mapall,color_map;
    cv::Mat ir_pinhole_exRT=cv::Mat::eye(4, 4, CV_64F);
    cv::Mat irproject,frontproject=cv::Mat::eye(3, 3, CV_64F);
    cv::Mat irdistortion,frontdistortion;
    cv::Size imagesize,irimagesize;
    pcl::PointCloud<pcl::PointXYZ> ndtFrame,neighbors;
    Eigen::Matrix4d fix_lidar_pose;//有的时候安装为了躲开机械臂会有角度，x并不是向前
    float fix_rotate_z=-131.0f;
    float fix_move_x=0.28f;
};
#endif
