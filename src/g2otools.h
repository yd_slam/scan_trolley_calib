#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <dirent.h>
#include <Eigen/Core>
#include "eigen3/Eigen/Dense"
//g2o
#include <g2o/core/base_vertex.h>
#include <g2o/core/base_binary_edge.h>
#include <g2o/types/slam3d/vertex_se3.h>
#include <g2o/core/block_solver.h>
#include <g2o/core/optimization_algorithm_levenberg.h>
#include <g2o/solvers/dense/linear_solver_dense.h>
class EdgeRobot : public g2o::BaseBinaryEdge<6, Eigen::Isometry3d, g2o::VertexSE3, g2o::VertexSE3>
{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    EdgeRobot(const double &angle, const Eigen::Isometry3d &ref) : BaseBinaryEdge<6, Eigen::Isometry3d, g2o::VertexSE3, g2o::VertexSE3>()
    {
        _angle = angle;
        _ref = ref;
    }

    void computeError()
    {
        Eigen::Isometry3d t1 = (static_cast<const g2o::VertexSE3 *>(_vertices[0]))->estimate();
        Eigen::Isometry3d t2 = Eigen::Isometry3d::Identity();
        t2.linear() = Eigen::AngleAxisd(_angle, Eigen::Vector3d::UnitZ()).toRotationMatrix();
        Eigen::Isometry3d t3 = (static_cast<const g2o::VertexSE3 *>(_vertices[1]))->estimate();
        t3.translation()[1] = 0;
        t3.translation()[2] = 0;
        Eigen::Isometry3d delta = t1 * t2 * t3 * (_measurement.inverse() * _ref);
        _error = g2o::internal::toVectorMQT(delta);
        _error *= std::exp(std::max(-1.0 * t3.translation()[0], 0.0));
    }

    void setMeasurement(const Eigen::Isometry3d &m)
    {
        _measurement = m;
    }

    virtual bool read(std::istream &is) {}
    virtual bool write(std::ostream &os) const {}

private:
    double _angle;
    Eigen::Isometry3d _ref;
};
std::string SaveAxisFile0(std::string outputpath
        ,float angle0,cv::Mat m0
        ,float angle1,cv::Mat m1
        ,float angle2,cv::Mat m2)
{
    std::string path_filename_str;
    path_filename_str = outputpath+"/extrinsics_0.yaml";

    cv::FileStorage fs(path_filename_str.c_str(), cv::FileStorage::WRITE);
    if (!fs.isOpened())
    {
        fprintf(stderr, "%s : cannot open file\n", path_filename_str.c_str());
        exit(EXIT_FAILURE);
    }
    m0.convertTo(m0, CV_64FC1);
    m1.convertTo(m1, CV_64FC1);
    m2.convertTo(m2, CV_64FC1);
    fs << "num" << 3;
    fs << "radian_0_0" << angle0/180.0f*M_PI;
    fs << "radian_0_1" << angle1/180.0f*M_PI;
    fs << "radian_0_2" << angle2/180.0f*M_PI;
    fs << "CameraExtrinsicMat_0_0" <<m0 ;
    fs << "CameraExtrinsicMat_0_1" <<m1 ;
    fs << "CameraExtrinsicMat_0_2" <<m2 ;
    std::cout<<"wroten yaml file in: "<<path_filename_str<<std::endl;
    return path_filename_str;
}
std::string SaveAxisFile1(std::string outputpath,float centerangleH
        ,float angle0,cv::Mat m0
        ,float angle1,cv::Mat m1
        ,float angle2,cv::Mat m2)
{
    std::string path_filename_str;
    path_filename_str = outputpath+"/extrinsics_1.yaml";

    cv::FileStorage fs(path_filename_str.c_str(), cv::FileStorage::WRITE);
    if (!fs.isOpened())
    {
        fprintf(stderr, "%s : cannot open file\n", path_filename_str.c_str());
        exit(EXIT_FAILURE);
    }
    m0.convertTo(m0, CV_64FC1);
    m1.convertTo(m1, CV_64FC1);
    m2.convertTo(m2, CV_64FC1);
    fs << "radian_0" << centerangleH/180.0f*M_PI;
    fs << "num" << 3;
    fs << "radian_1_0" << angle0/180.0f*M_PI;
    fs << "radian_1_1" << angle1/180.0f*M_PI;
    fs << "radian_1_2" << angle2/180.0f*M_PI;
    fs << "CameraExtrinsicMat_1_0" <<m0 ;
    fs << "CameraExtrinsicMat_1_1" <<m1 ;
    fs << "CameraExtrinsicMat_1_2" <<m2 ;
    std::cout<<"wroten yaml file in: "<<path_filename_str<<std::endl;
    return path_filename_str;
}
bool ReadFile(std::string config_file, std::vector<Eigen::Isometry3d> &se3s, std::vector<double> &radian, int axis_index, std::vector<double> &refrads, Eigen::Vector3d &trans_world)
{
    se3s.clear();
    radian.clear();
    refrads.clear();

    cv::FileStorage fsSettings(config_file, cv::FileStorage::READ);
    if (!fsSettings.isOpened())
    {
        std::cerr << "readParameters ERROR: Wrong setting path for axis " << axis_index << std::endl;
        return false;
    }
    int num = fsSettings["num"];
    if (num < 3)
    {
        std::cerr << "readParameters ERROR: Observation number for axis " << axis_index << " should not be less than 3" << std::endl;
    }
    for (int i = 0; i < num; ++i)
    {
        cv::Mat cv_RT;
        std::string index("CameraExtrinsicMat_");
        index = index + std::to_string(axis_index) + "_" + std::to_string(i);
        fsSettings[index] >> cv_RT;
        Eigen::Matrix4d eigen_RT;
        cv::cv2eigen(cv_RT, eigen_RT);
        //wxyadd
        if (axis_index == 0 && i == 0)
        {
            trans_world << eigen_RT(0, 3), eigen_RT(1, 3), eigen_RT(2, 3);
            //std::cout << "trans_world:" << trans_world << std::endl;
        }


        Eigen::Matrix3d so3_0(eigen_RT.block<3, 3>(0, 0));
        Eigen::Vector3d trans_0(eigen_RT(0, 3) - trans_world(0), eigen_RT(1, 3) - trans_world(1), eigen_RT(2, 3) - trans_world(2));
        Eigen::Isometry3d se3 = Eigen::Isometry3d::Identity();
        se3.linear() = so3_0;
        se3.translation() = trans_0;
        se3s.push_back(se3);

        std::string ind("radian_");
        ind = ind + std::to_string(axis_index) + "_" + std::to_string(i);
        double rad;
        fsSettings[ind] >> rad;
        radian.push_back(rad);
    }
    if (axis_index > 0)
    {
        for (int i = 0; i < axis_index; ++i)
        {
            std::string ind("radian_");
            ind = ind + std::to_string(i);
            double rad;
            fsSettings[ind] >> rad;
            refrads.push_back(rad);
        }
    }
    return true;
}
Eigen::Isometry3d GetRefSE3(const std::vector<Eigen::Isometry3d> &frameseries, const std::vector<double> &distanceseries)
{
    assert(frameseries.size() == distanceseries.size());
    int num = frameseries.size();
    Eigen::Isometry3d refinseries = Eigen::Isometry3d::Identity();
    Eigen::Isometry3d motionframes;
    for (int i = 0; i < num; i++)
    {
        motionframes = Eigen::Isometry3d::Identity();
        motionframes.linear() = Eigen::AngleAxisd(distanceseries[i], Eigen::Vector3d::UnitZ()).toRotationMatrix();
        motionframes.translation() = Eigen::Vector3d(0, 0, 0);
        refinseries = refinseries * frameseries[i] * motionframes;
    }
    return refinseries;
}
bool GetSE3ByPoints(const Eigen::Isometry3d &ref, const std::vector<Eigen::Isometry3d> &se3s, const std::vector<double> &radian, Eigen::Isometry3d &t1, Eigen::Isometry3d &t2, double &error)
{
    int num_p = se3s.size();
    assert(num_p >= 3 && num_p == int(radian.size()));
    g2o::SparseOptimizer optimizer;
    g2o::OptimizationAlgorithmLevenberg *solver = new g2o::OptimizationAlgorithmLevenberg(g2o::make_unique<g2o::BlockSolverX>(g2o::make_unique<g2o::LinearSolverDense<g2o::BlockSolverX::PoseMatrixType>>()));
    optimizer.setAlgorithm(solver);

    g2o::VertexSE3 *v1 = new g2o::VertexSE3();
    g2o::VertexSE3 *v2 = new g2o::VertexSE3();
    v1->setEstimate(t1);
    v1->setId(0);
    optimizer.addVertex(v1);
    v2->setEstimate(t2);
    v2->setId(1);
    optimizer.addVertex(v2);

    for (int i = 0; i < se3s.size(); i++)
    {
        EdgeRobot *edge = new EdgeRobot(radian[i], ref);
        edge->setId(i);
        edge->setVertex(0, v1);
        edge->setVertex(1, v2);
        edge->setMeasurement(se3s[i]);
        Eigen::Matrix<double, 6, 6> temp = Eigen::Matrix<double, 6, 6>::Identity();
        //012是平移权重
        // temp(0, 0) = 0.01;
        // temp(1, 1) = 0.01;
        // temp(2, 2) = 0.01;
        edge->setInformation(temp);
        optimizer.addEdge(edge);
    }
    optimizer.initializeOptimization();
    optimizer.optimize(200);

    t1 = v1->estimate();
    t2 = v2->estimate();
    error = optimizer.chi2();
    error = std::sqrt(error);
    // std::cout << v1->estimate().matrix() << std::endl;
    // std::cout << v2->estimate().matrix() << std::endl;
    // std::cout << optimizer.chi2() << std::endl;
    return true;
}