#include "axiscalibor.h"
#include "tools.h"
#include "g2otools.h"
#define NONE       "\e[0m"
#define L_RED      "\e[1;31m"
#define L_GREEN    "\e[1;32m"
#define printlr(format, arg...) do{printf(L_RED format NONE,## arg);}while(0)
#define printlg(format, arg...) do{printf(L_GREEN format NONE,## arg);}while(0)
bool CalibAxis::Init(std::string _datapath,std::string _predatapath,std::string _outputdatapath) {
    datapath=_datapath;
    outputdatapath=_outputdatapath;
    predatapath=_predatapath;
    std::vector<std::string> anglefiles;
    ReadSonFileList(_datapath,anglefiles);
    if(!CollectInfo(_datapath,anglefiles,DataInfoC,DataInfo2H,DataInfo2V))
        return false;
    std::cout<<"  Loading map.ply ... ..."<<std::endl;
    if (pcl::io::loadPLYFile("../Map/map.ply", color_mapall) == -1)
    {
        printlr("Inputfile Erorr: load map from ../Map failed!\n");
        return false;
    }
    UniformSamplingfilter_RGB(color_mapall,0.05,color_map);
    pcl::copyPointCloud(color_map,map);
    ir_pinhole_yaml=datapath+"/xjcar_ircam_pinhole.yaml";
    ReadCalibrationFile(ir_pinhole_yaml,ir_pinhole_exRT,irproject,irdistortion,irimagesize);

    //机械臂车
    Eigen::AngleAxisd yawAngle(Eigen::AngleAxisd(fix_rotate_z/180.0*M_PI, Eigen::Vector3d::UnitZ())); //z向前
    Eigen::Matrix3d rotation_matrix=yawAngle.toRotationMatrix();
    Eigen::Vector3d neweulerAngle = rotation_matrix.eulerAngles(2, 1, 0);
    cv::Mat handin_R;
    cv::Mat adjustmatrix=cv::Mat::eye(4, 4, CV_64F);
    cv::eigen2cv(rotation_matrix, handin_R);
    handin_R.copyTo(adjustmatrix(cv::Rect_<double>(0, 0, 3, 3)));
    adjustmatrix.at<double>(0, 3) = fix_move_x;
    adjustmatrix.at<double>(1, 3) = 0.0;
    adjustmatrix.at<double>(2, 3) = 0.0;
    adjustmatrix.at<double>(3, 3) = 1.0;
    cv::cv2eigen(adjustmatrix, fix_lidar_pose);

    printlg("Init successed!\n");
    return true;
}
bool CalibAxis::CalibEveryExRT(DataInfo &inputdatainfo)
{
    std::vector<std::string> pcdfilenames;
    std::string pcdfilepath = datapath +"/"+int2str(inputdatainfo.H)+"_"+int2str(inputdatainfo.V)+"/pcd";
    printlg("calibrating %s_%s ...\n",int2str(inputdatainfo.H).c_str(),int2str(inputdatainfo.V).c_str());
    Eigen::Matrix4d inittrans=inputdatainfo.lidar_map_pose;

    Eigen::Affine3d temp;
    if(YDGetPCDDataFilePath(pcdfilepath, pcdfilenames)==0)
    {
        printlr("Inputfile Erorr: velodyne pcd sum is wrong!\n");
        return false;
    }
    for (int i = 0; i < (pcdfilenames.size()>15?15:pcdfilenames.size());i++)
    {
        if (pcl::io::loadPCDFile(pcdfilenames[i], pc_temp) == -1)
        {
            PCL_ERROR("Couldn't read Velodyne pcd files\n");
            continue;
        }
        lidar_frame.insert(lidar_frame.end(), pc_temp.begin(), pc_temp.end());
    }
    pcl::PointCloud<pcl::PointXYZ> lidar_in_map,fix_lidar_new;
    pcl::transformPointCloud(lidar_frame, fix_lidar_new, fix_lidar_pose);//结构固定变换
    pcl::transformPointCloud(fix_lidar_new, lidar_in_map, inittrans);//雷达到地图的初始
    getneighbors(map,lidar_in_map,neighbors);
    Eigen::Matrix<double, 4, 4> ndt_trans_d;
    cv::Mat ndtmatrix = cv::Mat::eye(4, 4, CV_64F);
    double error=NDT_Process(lidar_in_map,neighbors,ndt_trans_d);
    cv::eigen2cv(ndt_trans_d,ndtmatrix);
    PrintEular(ndtmatrix);
    pcl::transformPointCloud(lidar_in_map,ndtFrame,ndt_trans_d);
    pcl::io::savePCDFile(outputdatapath+"/"+int2str(inputdatainfo.H)+"_"+int2str(inputdatainfo.V)+"_fix_lidar_new.pcd", fix_lidar_new);
    pcl::io::savePLYFile(outputdatapath+"/"+int2str(inputdatainfo.H)+"_"+int2str(inputdatainfo.V)+"_ndtFrame.ply", ndtFrame);
    pcl::io::savePLYFile(outputdatapath+"/"+int2str(inputdatainfo.H)+"_"+int2str(inputdatainfo.V)+"_robotinitpose.ply", lidar_in_map);
    ndtFrame.resize(0);
    //重新计算新的可见光雷达外参
    cv::Mat lidar_map_exRT=cv::Mat::eye(4, 4, CV_64F);
    cv::eigen2cv(inputdatainfo.lidar_map_pose,lidar_map_exRT);

    cv::Mat cam_lidar_exRT=cv::Mat::eye(4, 4, CV_64F);
    CalibNewcam_lidar(inputdatainfo.frame_map_exRT,ndtmatrix,lidar_map_exRT,cam_lidar_exRT);
    inputdatainfo.frame_lidar_exRT=cam_lidar_exRT.clone();
    cout<<"cam_lidar_exRT's "<<endl;
    PrintEular(cam_lidar_exRT);
    pc_temp.clear();
    lidar_frame.clear();
    return true;
}
bool CalibAxis::Calibration()
{
    if(!CalibEveryExRT(DataInfoC[0])||
    !CalibEveryExRT(DataInfo2H[0])||
    !CalibEveryExRT(DataInfo2H[1])||
    !CalibEveryExRT(DataInfo2V[0])||
    !CalibEveryExRT(DataInfo2V[1]))
        return false;
    std::vector<std::string> g2oyamlfilepath;
    g2oyamlfilepath.resize(0);
    //保存0轴yaml文件到本地
    g2oyamlfilepath.push_back(SaveAxisFile0(outputdatapath,DataInfoC[0].H,DataInfoC[0].frame_lidar_exRT,
                                         DataInfo2H[0].H,DataInfo2H[0].frame_lidar_exRT,
                                         DataInfo2H[1].H,DataInfo2H[1].frame_lidar_exRT));
    //保存1轴yaml文件到本地
    g2oyamlfilepath.push_back(SaveAxisFile1(outputdatapath,DataInfoC[0].H,
                                            DataInfoC[0].V,DataInfoC[0].frame_lidar_exRT,
                                            DataInfo2V[0].V,DataInfo2V[0].frame_lidar_exRT,
                                            DataInfo2V[1].V,DataInfo2V[1].frame_lidar_exRT));
    printlg("calibrating rotate axis ...\n");
    std::vector<Eigen::Isometry3d> se3s;
    std::vector<double> radian;
    std::vector<Eigen::Isometry3d> refs;
    std::vector<double> refrads;
    Eigen::Vector3d trans_world;
    int num =2;
    std::string fileto=outputdatapath+"/parameters.yaml";
    cv::FileStorage fwrite(fileto, cv::FileStorage::WRITE);
    fwrite << "num" << num;
    for (int i = 0; i < num; ++i) {
        Eigen::Isometry3d ref = Eigen::Isometry3d::Identity();
        bool readres = ReadFile(g2oyamlfilepath[i], se3s, radian, i, refrads, trans_world);
        if (!readres) {
            std::cout << "Read file " << g2oyamlfilepath[i] << " failed!" << std::endl;
            return false;
        }
        ref = GetRefSE3(refs, refrads);
        Eigen::Isometry3d t11 = Eigen::Isometry3d::Identity();
        Eigen::Isometry3d t22 = Eigen::Isometry3d::Identity();
        double error = 1e10;

        Eigen::Isometry3d t1_temp = Eigen::Isometry3d::Identity();
        Eigen::Isometry3d t2_temp = Eigen::Isometry3d::Identity();
        double error_temp;
        for (int j = 0; j < 4; ++j)
        {
            for (int k = 0; k < 4; ++k)
            {
                for (int l = 0; l < 4; ++l)
                {
                    t1_temp = Eigen::Isometry3d::Identity();
                    t1_temp.linear() = (Eigen::AngleAxisd(1.5707963267948966 * j, Eigen::Vector3d::UnitZ()) *
                                        Eigen::AngleAxisd(0.7853981633974483 * k, Eigen::Vector3d::UnitY()) * Eigen::AngleAxisd(0.7853981633974483 * l, Eigen::Vector3d::UnitX()))
                            .toRotationMatrix();
                    for (int m = 0; m < 4; ++m)
                    {
                        for (int n = 0; n < 4; ++n)
                        {
                            for (int o = 0; o < 4; ++o)
                            {
                                t2_temp = Eigen::Isometry3d::Identity();
                                t2_temp.linear() = (Eigen::AngleAxisd(0.7853981633974483 * m, Eigen::Vector3d::UnitZ()) *
                                                    Eigen::AngleAxisd(0.7853981633974483 * n, Eigen::Vector3d::UnitY()) * Eigen::AngleAxisd(0.7853981633974483 * o, Eigen::Vector3d::UnitX()))
                                        .toRotationMatrix();

                                GetSE3ByPoints(ref, se3s, radian, t1_temp, t2_temp, error_temp);
                                if (error_temp < error)
                                {
                                    t11 = t1_temp;
                                    t22 = t2_temp;
                                    error = error_temp;
                                }
                            }
                        }
                    }
                }
            }
        }

        t22.translation()[1] = 0;
        t22.translation()[2] = 0;
        cv::Mat cvmat;
        cv::eigen2cv(t11.matrix(), cvmat);
        std::stringstream ss;
        ss << "Error_" << i;
        fwrite << ss.str() << error;
        ss.str("");
        ss << "Matrix_" << i;
        if (i == 0)
        {
            //std::cout << trans_world << std::endl;
            cvmat.at<double>(0, 3) += trans_world(0);
            cvmat.at<double>(1, 3) += trans_world(1);
            cvmat.at<double>(2, 3) += trans_world(2);
        }
        fwrite << ss.str() << cvmat;
        std::cout << "Rotation " << i << ":" << std::endl
                  << t11.rotation()
                  << std::endl
                  << "Translation " << i << ":" << std::endl
                  << t11.translation().transpose()
                  << std::endl;
        printlg(" Erorr %d : %f\n",i,error);


        if (i == num - 1)
        {
            cv::Mat cvtool;
            cv::eigen2cv(t22.matrix(), cvtool);
            fwrite << "Tool" << cvtool;
            std::cout << "Tool Rotation:" << std::endl
                      << t22.linear()
                      << std::endl
                      << "Tool Translation:" << std::endl
                      << t22.translation().transpose() << std::endl;
        }

        refs.push_back(t11);
    }
    fwrite << "CameraExtrinsicMat"<<cv::Mat::eye(4, 4, CV_64F);
    fwrite << "IRColorCameraExtrinsicMat"<<ir_pinhole_exRT;
    fwrite << "IRCameraMat"<<irproject;
    fwrite << "IRDistCoeff"<<irdistortion;
    fwrite << "IRImageSize"<<irimagesize;
    fwrite << "CameraMat"<<frontproject;
    fwrite << "DistCoeff"<<frontdistortion;
    fwrite << "ImageSize"<<imagesize;

    return true;
}
void  CalibAxis::CalibNewcam_lidar(cv::Mat cam_map_exRT,cv::Mat lidar_map_ndt,cv::Mat lidar_map_exRT,cv::Mat &cam_lidar_exRT)
{
    Eigen::Matrix4d transform;
    Eigen::Affine3d temp;
    Eigen::Isometry3d RT1_trans = Eigen::Isometry3d::Identity();
    Eigen::Isometry3d RT2_trans = Eigen::Isometry3d::Identity();
    Eigen::Isometry3d RT3_trans = Eigen::Isometry3d::Identity();

    cv::Mat map_cam_exRT=cv::Mat::eye(4, 4, CV_64F);
    cv::cv2eigen(cam_map_exRT, transform);
    temp.matrix() = transform;
    transform = temp.inverse().matrix();
    cv::eigen2cv(transform,map_cam_exRT);

    cv::cv2eigen(lidar_map_exRT, RT1_trans.matrix());
    cv::cv2eigen(lidar_map_ndt, RT2_trans.matrix());
    cv::cv2eigen(map_cam_exRT, RT3_trans.matrix());

    Eigen::Isometry3d lidar_cam_eigen =  RT3_trans*RT2_trans * RT1_trans;
    cv::eigen2cv(lidar_cam_eigen.matrix(), cam_lidar_exRT);
    cv::cv2eigen(cam_lidar_exRT, transform);
    temp.matrix() = transform;
    transform = temp.inverse().matrix();
    cv::eigen2cv(transform, cam_lidar_exRT);
}
double CalibAxis::NDT_Process(pcl::PointCloud<pcl::PointXYZ> &Input, pcl::PointCloud<pcl::PointXYZ> &Target, Eigen::Matrix4d &trans_d)
{
    double dur;
    clock_t start, end;
    start = clock();
    pcl::PointCloud<pcl::PointXYZ>::Ptr Inputpt(new pcl::PointCloud<pcl::PointXYZ>(Input));
    pcl::PointCloud<pcl::PointXYZ>::Ptr Targetptr(new pcl::PointCloud<pcl::PointXYZ>(Target));
    pclomp::NormalDistributionsTransform<pcl::PointXYZ, pcl::PointXYZ>::Ptr ndt(new pclomp::NormalDistributionsTransform<pcl::PointXYZ, pcl::PointXYZ>());
    ndt->setNeighborhoodSearchMethod(pclomp::DIRECT7);
    ndt->setNumThreads(5);
    ndt->setTransformationEpsilon(0.00000001);
    ndt->setStepSize(0.2);
    ndt->setResolution(0.05);
    ndt->setMaximumIterations(20);
    ndt->setInputSource(Inputpt);   //第二次扫描的点云
    ndt->setInputTarget(Targetptr); //第一次扫描的点云
    pcl::PointCloud<pcl::PointXYZ>::Ptr output_cloud(new pcl::PointCloud<pcl::PointXYZ>);
    Eigen::Matrix4f init_guess = Eigen::Matrix4f::Identity();
    ndt->align(*output_cloud, init_guess);
    end = clock();
    dur = (double)(end - start);
    printf("  ndt uses Time:%f ms", (dur / CLOCKS_PER_SEC));
    std::cout << " score: " << ndt->getFitnessScore() << std::endl;
    pcl::transformPointCloud(*Inputpt, *output_cloud, ndt->getFinalTransformation());
    Eigen::Matrix4f transformation = ndt->getFinalTransformation();
    M4ftoM4d(transformation, trans_d);
    return ndt->getFitnessScore();
}
bool CalibAxis::CollectInfo(std::string _datapath,std::vector<std::string> _anglefiles,
                            std::vector<DataInfo> &_DataInfoC,std::vector<DataInfo> &DataInfo2H,std::vector<DataInfo> &DataInfo2V)
{
    //choose center pose
    DataInfo datainfo;
    std::vector<int> Hs;
    std::vector<int> Vs;
    Hs.resize(0);
    Vs.resize(0);
    std::vector<string> yamls;
    int Cpose_H=0;
    int Cpose_V=0;
    int sum=_anglefiles.size();
    for(int i=0;i<sum;i++)
    {
        int h,v;
        sscanf(_anglefiles[i].c_str(), "%d_%d", &h, &v);
        h=(int)h;
        v=(int)v;
        Hs.push_back(h);
        Vs.push_back(v);
    }
    for(int m=0;m<sum;m++)
    {
        int temph=Hs[m];
        int tempv=Vs[m];
        int hcnt=0;
        int vcnt=0;
        for(int n=0;n<sum;n++) {
            if (temph == Hs[n])
                hcnt = hcnt+1;
            if(tempv==Vs[n])
                vcnt+=1;
        }
        if(hcnt==3)
            Cpose_H=temph;
        if(vcnt==3)
            Cpose_V=tempv;
    }
    cout<<"  center pose is "<<Cpose_H<<" "<<Cpose_V<<endl;
    std::string yamlfilename=predatapath+"/"+int2str(Cpose_H)+"_"+int2str(Cpose_V);
    std::string posetxtfile=datapath+"/"+int2str(Cpose_H)+"_"+int2str(Cpose_V)+"/pose.txt";
    if(YDGetYAMLFilePath(yamlfilename,yamls)!=1)
    {
        printlr("%s 's yaml count is not 1",yamlfilename.c_str());
        return false;
    }
    yamlfilename=yamls[0];
    yamls.clear();
    cv::Mat tempexRT;
    ReadCalibrationFile(yamlfilename,tempexRT,frontproject, frontdistortion, imagesize);
    datainfo.frame_map_exRT=tempexRT.clone();
    readposematrix(posetxtfile, datainfo.lidar_map_pose);
    datainfo.H=Cpose_H;
    datainfo.V=Cpose_V;
    _DataInfoC.push_back(datainfo);
    for(int j=0;j<sum;j++)
    {
        //找出水平的另外两个电机位置
        if(Hs[j]!=Cpose_H&&Vs[j]==Cpose_V)
        {
            yamlfilename=predatapath+"/"+int2str(Hs[j])+"_"+int2str(Vs[j]);
            if(YDGetYAMLFilePath(yamlfilename,yamls)!=1)
            {
                printlr("%s 's yaml count is not 1",yamlfilename.c_str());
                return false;
            }
            yamlfilename=yamls[0];
            yamls.clear();
            cv::Mat tempexRT;
            ReadCalibrationFile(yamlfilename,tempexRT,frontproject, frontdistortion, imagesize);
            datainfo.frame_map_exRT=tempexRT.clone();
            posetxtfile=datapath+"/"+int2str(Hs[j])+"_"+int2str(Vs[j])+"/pose.txt";
            readposematrix(posetxtfile, datainfo.lidar_map_pose);
            datainfo.H=Hs[j];
            datainfo.V=Vs[j];
            DataInfo2H.push_back(datainfo);
        }
        //找出垂直的另外两个电机位置
        if(Hs[j]==Cpose_H&&Vs[j]!=Cpose_V)
        {
            yamlfilename=predatapath+"/"+int2str(Hs[j])+"_"+int2str(Vs[j]);
            if(YDGetYAMLFilePath(yamlfilename,yamls)!=1)
            {
                printlr("%s 's yaml count is not 1",yamlfilename.c_str());
                return false;
            }
            yamlfilename=yamls[0];
            yamls.clear();
            cv::Mat tempexRT;
            ReadCalibrationFile(yamlfilename,tempexRT,frontproject, frontdistortion, imagesize);
            datainfo.frame_map_exRT=tempexRT.clone();
            posetxtfile=datapath+"/"+int2str(Hs[j])+"_"+int2str(Vs[j])+"/pose.txt";
            readposematrix(posetxtfile, datainfo.lidar_map_pose);
            datainfo.H=Hs[j];
            datainfo.V=Vs[j];
            DataInfo2V.push_back(datainfo);
        }
    }
    return true;
}
