#include "optimtor.h"
#include "tools.h"
#define NONE       "\e[0m"
#define L_RED      "\e[1;31m"
#define L_GREEN    "\e[1;32m"
#define printlr(format, arg...) do{printf(L_RED format NONE,## arg);}while(0)
#define printlg(format, arg...) do{printf(L_GREEN format NONE,## arg);}while(0)
bool Optim::Init(std::string _datapath,std::string _predatapath,std::string _outputdatapath,std::string _devicetype) {
    datapath=_datapath;
    outputdatapath=_outputdatapath;
    std::string pcdfilepath = datapath + "/pcd";
    std::vector<std::string> pcdfilenames,imgfilenames;
    if(_devicetype.compare("o")==0)
        devicetypeid=0;
    else if(_devicetype.compare("p")==0)
        devicetypeid=1;
    else{
        printlr("Input Erorr: the device type is not surpport!\n");
        return false;
    }
    if(YDGetPCDDataFilePath(pcdfilepath, pcdfilenames)==0)
    {
        printlr("Inputfile Erorr: velodyne pcd sum is wrong!\n");
        return false;
    }
    for (int i = 0; i < (pcdfilenames.size()>5?5:pcdfilenames.size());i++)
    {
        if (pcl::io::loadPCDFile(pcdfilenames[i], pc_temp) == -1)
        {
            PCL_ERROR("Couldn't read Velodyne pcd files\n");
            continue;
        }
        lidar_frame.insert(lidar_frame.end(), pc_temp.begin(), pc_temp.end());
    }
    std::cout<<"  loading map.ply ... ..."<<std::endl;
    if (pcl::io::loadPLYFile("../Map/map.ply", color_mapall) == -1)
    {
        printlr("Inputfile Erorr: load map from ../Map failed!\n");
        return false;
    }
    if(YDGetIMGFilePath(datapath,imgfilenames)!=1)
    {
        printlr("Inputfile Erorr: image sum is wrong!\n");
        return false;
    }


    UniformSamplingfilter_RGB(color_mapall,0.01,color_map);
    pcl::io::savePLYFile("../temp/map_s.ply", color_map);
    pcl::copyPointCloud(color_map,map);

    //if有广角
    if(devicetypeid==0){
        CutImage(imgfilenames[0], Frontimg, Rearimg);
        std::string omni_lidar_yaml=datapath+"/omni_lidar_calibration.yaml";
        std::string omni_map_yaml=_predatapath+"/omni_map_calibration.yaml";
        ReadCalibrationFile_omni(omni_map_yaml, omni_map_exRT, omniproject, omnidistortion, xi,imagesize);
        ReadCalibrationFile_omni(omni_lidar_yaml, omni_lidar_exRT, omniproject, omnidistortion, xi,imagesize);
        ReadKeysInfo(_predatapath+"/omni_keys2d3d.txt",Okeys2D,OKeys3D);

        std::string front_lidar_yaml=datapath+"/front_fisheye_lidar_calibration.yaml";
        std::string front_map_yaml=_predatapath+"/front_map_calibration.yaml";
        ReadCalibrationFile(front_map_yaml, front_map_exRT, frontproject, frontdistortion, imagesize);
        ReadCalibrationFile(front_lidar_yaml, front_lidar_exRT, frontproject, frontdistortion, imagesize);
        ReadKeysInfo(_predatapath+"/front_keys2d3d.txt",Fkeys2D,Fkeys3D);
    }
    else if(devicetypeid==1)
    {
        //if 针孔
        Frontimg=cv::imread(imgfilenames[0]);
        std::vector<std::string> yamlfilenames;
        int yamlfilecnt=YDGetYAMLFilePath(datapath,yamlfilenames);
        std::string front_lidar_yaml=yamlfilenames[0];
        std::string front_map_yaml=_predatapath+"/front_map_calibration.yaml";
        ReadCalibrationFile(front_map_yaml, front_map_exRT, frontproject, frontdistortion, imagesize);
        ReadCalibrationFile(front_lidar_yaml, front_lidar_exRT, frontproject, frontdistortion, imagesize);
        ReadKeysInfo(_predatapath+"/front_keys2d3d.txt",Fkeys2D,Fkeys3D);

    }


    printlg("Init successed!\n");
    return true;
}

bool Optim::Optimization()
{
    double error;
    if(devicetypeid==0) {
        error = OptimizeByMap("omni", omni_lidar_exRT, omni_map_exRT, optim_omni_lidar_exRT);
        saveCalibrationFile_omni(optim_omni_lidar_exRT, omniproject, omnidistortion, xi, imagesize, error,
                                 outputdatapath, "optim_omni_lidar");
        error = OptimizeByMap("front", front_lidar_exRT, front_map_exRT, optim_front_lidar_exRT);
        saveCalibrationFile(optim_front_lidar_exRT, frontproject, frontdistortion, imagesize, error, outputdatapath,
                            "optim_front_lidar");
    }
    else if(devicetypeid==1)
    {
        error = OptimizeByMap("front", front_lidar_exRT, front_map_exRT, optim_front_lidar_exRT);
        saveCalibrationFile(optim_front_lidar_exRT, frontproject, frontdistortion, imagesize, error, outputdatapath,
                            "optim_front_lidar");
    }
    return true;
}
double Optim::OptimizeByMap(std::string name,
                            cv::Mat cam_lidar_exRT,cv::Mat cam_map_exRT,cv::Mat &newEXMat)
{
    printlg("Optimizing %s_lidar_exRT...\n",name.c_str());
    pcl::PointCloud<pcl::PointXYZ> frame_in_cam,frame_in_map,disframe;
    Eigen::Matrix4f transform;
    Eigen::Affine3f temp;
    cv::cv2eigen(cam_lidar_exRT, transform);
    temp.matrix() = transform;
    transform = temp.inverse().matrix();
    pcl::transformPointCloud(lidar_frame, frame_in_cam, transform);//雷达坐标系到相机坐标系
    cv::cv2eigen(cam_map_exRT, transform);
    pcl::transformPointCloud(frame_in_cam, frame_in_map, transform);//再到世界坐标系
    //ndt配准
    Eigen::Matrix<double, 4, 4> ndt_trans_d;
    cv::Mat ndtmatrix = cv::Mat::eye(4, 4, CV_64F);
    pcl::io::savePLYFile("../temp/"+name+"_frame_in_map.ply", frame_in_map);
//    getneighbors(map,frame_in_map,neighbors);
//    pcl::io::savePLYFile("../temp/"+name+"_near_map.ply", neighbors);
//    UniformSamplingfilter(frame_in_map,0.05f,disframe);
    fardisfilter(frame_in_map,disframe,5.0f);
    double error=NDT_Process(disframe,map,ndt_trans_d);
    neighbors.resize(0);
//    double error=NDT_Process(frame_in_map,map,ndt_trans_d);
    cv::eigen2cv(ndt_trans_d,ndtmatrix);
    PrintEular(ndtmatrix);
    pcl::transformPointCloud(disframe,ndtFrame,ndt_trans_d);
    pcl::io::savePLYFile("../temp/"+name+"_ndtFrame.ply", ndtFrame);
    ndtFrame.resize(0);
    //重新计算新的可见光雷达外参
    CalibNewcam_lidar(cam_lidar_exRT,cam_map_exRT,ndtmatrix,newEXMat,lidar_map_exRT);
    return error;
}
void Optim::CalibNewcam_lidar(cv::Mat cam_lidar_exRT,cv::Mat cam_map_exRT,cv::Mat ndtMat,cv::Mat &newEXMat,cv::Mat &lidar_map_exRT)
{
    Eigen::Isometry3d RT1_trans = Eigen::Isometry3d::Identity();
    Eigen::Isometry3d RT2_trans = Eigen::Isometry3d::Identity();
    Eigen::Isometry3d RT3_trans = Eigen::Isometry3d::Identity();
    Eigen::Isometry3d RT4_trans = Eigen::Isometry3d::Identity();
    cv::Mat lidar_cam_exRT= cv::Mat::eye(4, 4, CV_64F);
    cv::Mat map_cam_exRT= cv::Mat::eye(4, 4, CV_64F);
    Eigen::Matrix4f transform;
    Eigen::Affine3f temp;

    cv::cv2eigen(cam_lidar_exRT, transform);
    temp.matrix() = transform;
    transform = temp.inverse().matrix();
    cv::eigen2cv(transform,lidar_cam_exRT);

    cv::cv2eigen(cam_map_exRT, transform);
    temp.matrix() = transform;
    transform = temp.inverse().matrix();
    cv::eigen2cv(transform,map_cam_exRT);

    cv::cv2eigen(lidar_cam_exRT, RT1_trans.matrix());
    cv::cv2eigen(cam_map_exRT, RT2_trans.matrix());
    cv::cv2eigen(ndtMat, RT3_trans.matrix());
    cv::cv2eigen(map_cam_exRT, RT4_trans.matrix());
    Eigen::Isometry3d delta = RT4_trans * RT3_trans*RT2_trans * RT1_trans;
    cv::eigen2cv(delta.matrix(), newEXMat);
    cv::cv2eigen(newEXMat, transform);
    temp.matrix() = transform;
    transform = temp.inverse().matrix();
    cv::eigen2cv(transform, newEXMat);

    Eigen::Isometry3d delta1 = RT3_trans*RT2_trans * RT1_trans;
    cv::eigen2cv(delta1.matrix(), lidar_map_exRT);

}

int Optim::ReadKeysInfo(std::string txtfilename,std::vector<cv::Point2d> &keys2D,std::vector<cv::Point3d> &keys3D)
{
    ifstream _File(txtfilename);
    int n=0;
    while (!_File.eof())
    {
        cv::Point2d _2dk;
        cv::Point3d _3dk;
        _File >>_2dk.x >> _2dk.y >> _3dk.x>> _3dk.y>>_3dk.z;
        keys2D.push_back(_2dk);
        keys3D.push_back(_3dk);
        n++;
    }
    _File.close();
    return n;
}

double Optim::NDT_Process(pcl::PointCloud<pcl::PointXYZ> &Input, pcl::PointCloud<pcl::PointXYZ> &Target, Eigen::Matrix4d &trans_d)
{
    double dur;
    clock_t start, end;
    start = clock();
    pcl::PointCloud<pcl::PointXYZ>::Ptr Inputpt(new pcl::PointCloud<pcl::PointXYZ>(Input));
    pcl::PointCloud<pcl::PointXYZ>::Ptr Targetptr(new pcl::PointCloud<pcl::PointXYZ>(Target));
    pclomp::NormalDistributionsTransform<pcl::PointXYZ, pcl::PointXYZ>::Ptr ndt(new pclomp::NormalDistributionsTransform<pcl::PointXYZ, pcl::PointXYZ>());
    ndt->setNeighborhoodSearchMethod(pclomp::DIRECT7);
    ndt->setNumThreads(5);
    ndt->setTransformationEpsilon(0.00000001);
    ndt->setStepSize(0.5);
    ndt->setResolution(0.05);
    ndt->setMaximumIterations(30);
    ndt->setInputSource(Inputpt);   //第二次扫描的点云
    ndt->setInputTarget(Targetptr); //第一次扫描的点云
    pcl::PointCloud<pcl::PointXYZ>::Ptr output_cloud(new pcl::PointCloud<pcl::PointXYZ>);
    Eigen::Matrix4f init_guess = Eigen::Matrix4f::Identity();
    ndt->align(*output_cloud, init_guess);
    end = clock();
    dur = (double)(end - start);
    printf("  ndt uses Time:%f ms", (dur / CLOCKS_PER_SEC));
    std::cout << " score: " << ndt->getFitnessScore() << std::endl;
    pcl::transformPointCloud(*Inputpt, *output_cloud, ndt->getFinalTransformation());
    Eigen::Matrix4f transformation = ndt->getFinalTransformation();
    M4ftoM4d(transformation, trans_d);
    return ndt->getFitnessScore();
}
void Optim::Render()
{
    Eigen::Matrix4d transform,transform_inv;
    Eigen::Affine3d temp;
    cv::Mat cam_map_exRT;
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr transformed_cloud(new pcl::PointCloud<pcl::PointXYZRGB>());
    pcl::PointCloud<pcl::PointXYZRGB> temprgbpc;
    //optim_rear_lidar_exRT,optim_front_lidar_exRT,lidar_map_exRT
    Eigen::Isometry3d RT1_trans = Eigen::Isometry3d::Identity();
    Eigen::Isometry3d RT2_trans = Eigen::Isometry3d::Identity();

    cv::cv2eigen(optim_front_lidar_exRT, RT1_trans.matrix());
    cv::cv2eigen(lidar_map_exRT, RT2_trans.matrix());
    Eigen::Isometry3d delta = RT2_trans * RT1_trans;
    cv::eigen2cv(delta.matrix(), cam_map_exRT);
    cv::cv2eigen(cam_map_exRT, transform);
    temp.matrix() = transform;
    transform_inv = temp.inverse().matrix();
    pcl::transformPointCloud(color_map, *transformed_cloud, transform_inv);
    RenderCheck(*transformed_cloud,transform,outputdatapath,frontproject, frontdistortion,Frontimg, cv::Rect(0, 0, imagesize.width, imagesize.height),"front",temprgbpc);
//    if(devicetypeid==0) {
//    cv::cv2eigen(optim_rear_lidar_exRT, RT1_trans.matrix());
//    Eigen::Isometry3d delta1 = RT2_trans * RT1_trans;
//    cv::eigen2cv(delta1.matrix(), cam_map_exRT);
//    cv::cv2eigen(cam_map_exRT, transform);
//    temp.matrix() = transform;
//    transform_inv = temp.inverse().matrix();
//
//    pcl::transformPointCloud(temprgbpc, *transformed_cloud, transform_inv);
//    RenderCheck(*transformed_cloud, transform,outputdatapath,rearproject, reardistortion,Rearimg,
//                cv::Rect(0, 0, imagesize.width, imagesize.height), "rear",temprgbpc);
//    }
    if(devicetypeid==0) {
        cv::cv2eigen(optim_omni_lidar_exRT, RT1_trans.matrix());
        Eigen::Isometry3d delta1 = RT2_trans * RT1_trans;
        cv::eigen2cv(delta1.matrix(), cam_map_exRT);
        cv::cv2eigen(cam_map_exRT, transform);
        temp.matrix() = transform;
        transform_inv = temp.inverse().matrix();

        pcl::transformPointCloud(color_map, *transformed_cloud, transform_inv);
        RenderCheck_omni(*transformed_cloud, transform, outputdatapath, omniproject, omnidistortion, xi, Frontimg,
                         cv::Rect(0, 0, imagesize.width, imagesize.height), "omni", temprgbpc);
    }
}