#include <iostream>
#include "camposecalib.h"
#include <sys/stat.h>
#include <sys/types.h>
using namespace std;
#define NONE       "\e[0m"
#define L_RED      "\e[1;31m"
#define L_GREEN    "\e[1;32m"
#define printlr(format, arg...) do{printf(L_RED format NONE,## arg);}while(0)
#define printlg(format, arg...) do{printf(L_GREEN format NONE,## arg);}while(0)
static void help()
{
    printf( "\nThis is a camera pose calibration for double fisheye camera or single camera's pose in CalibrationRoom.\n"
            "Usage: tagimgpos_calibr\n"
            "     -inputdatapath <the address of data files>         # include image and intrinsic yaml\n"
            "     -tagsize <AprilTags size (m)>                      # AprilTags size m\n"
            "     -device <d:double fisheye s:single camera o:omni>         # device type\n"
            "     -maptaglist <the address of tagslist.txt>          # the address of tagslist.txt\n"
            "     An Example:\n"
            "     ./tagimgpos_calibr -inputdatapath ../input -tagsize 0.16 -device d -maptaglist ../config/tagslist.txt\n"
            "\n" );

}
//
int main(int argc, char **argv)
{
    std::string outputpath=argv[0];

    std::string inputdatapath;
    std::string tagslisttxt;
    float tagsize;
    std::string devicetype;
    cv::CommandLineParser parser(argc, argv,"{help||}"
                                            "{@inputdatapath||}{@tagsize||}{@device||}{@maptaglist||}{angle||}{@anglename||}");
    if (parser.has("help"))
    {
        help();
        return 0;
    }
    if(parser.has("angle"))
    {
        outputpath="."+ outputpath+"_output/"+parser.get<std::string>("@anglename")+"/";
    }
    else
    {
        outputpath="."+ outputpath+"_output/";
    }
    cout<<"  build file"<<outputpath<<endl;
    mkdir(outputpath.c_str(),S_IRWXU);
    inputdatapath = parser.get<std::string>("@inputdatapath");
    tagsize = parser.get<float>("@tagsize");
    devicetype=parser.get<std::string>("@device");
    tagslisttxt=parser.get<std::string>("@maptaglist");

    JTCalib calibor;
    if(!calibor.Init(inputdatapath,outputpath,tagsize,devicetype,tagslisttxt))
        return -1;
    if(!calibor.DetectTool()) {
        printlr(" tags detected is not enough\n");
        return -1;
    }
    if(!calibor.Calibration())
        return -1;
    calibor.Render();
    return 0;
}
