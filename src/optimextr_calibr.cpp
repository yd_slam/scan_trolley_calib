#include <iostream>
#include "optimtor.h"
using namespace std;
static void help()
{
    printf( "\nThis is optimization tool to optimize the extrinsice between camera and lidar by localization in faro map.\n"
            "Usage: optimextr_calibr\n"
            "     -inputdatapath <the address of data files>         # include image and intrinsic yaml\n"
            "     -predatapath <the address of data files>           # tagimgpos_calibr products temp data\n"
            "     -device <p:pinehole  o:omni>                       # device type\n"
            "     An Example:\n"
            "     ./optimextr_calibr -inputdatapath ../input -predatapath ../tagimgpos_calibr_output -device o\n"
            "\n" );

}
//
int main(int argc, char **argv)
{
    std::string outputpath=argv[0];
    outputpath="."+ outputpath+"_output/";
    mkdir(outputpath.c_str(),S_IRWXU);
    std::string inputdatapath;
    std::string predatapath;
    std::string tagslisttxt;
    float tagsize;
    std::string devicetype;
    cv::CommandLineParser parser(argc, argv,"{help||}"
                                            "{@inputdatapath||}{@predatapath||}{@device||}");
    if (parser.has("help"))
    {
        help();
        return 0;
    }
    inputdatapath = parser.get<std::string>("@inputdatapath");
    predatapath=parser.get<std::string>("@predatapath");
    devicetype=parser.get<std::string>("@device");
    Optim optimor;
    if(!optimor.Init(inputdatapath,predatapath,outputpath,devicetype))
        return -1;
    optimor.Optimization();
    optimor.Render();
    return 0;
}